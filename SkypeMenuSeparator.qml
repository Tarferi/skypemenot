import QtQuick 2.0
import QtQuick.Controls 2.0

MenuItem {

    id:qitem

    property color realBackground: "#E2E2E2"
    property int realHeight: 6
    property int lineHeight: 1


    width: parent.width;
    height: realHeight;

    indicator: Rectangle {
        width:0
        height:0
        color: "transparent"
    }

    contentItem: Rectangle {
        width:0
        height:0
        color: "transparent"
    }

    background: Rectangle {
        anchors.fill: parent
        color: "transparent"
        Rectangle {
            width: parent.width
            height: lineHeight
            color: realBackground
            y: (realHeight)/2
        }
    }
}
