import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQml.Models 2.2

Item {

    property bool isActive: true
    property bool isMe: true

    property color defaultColor: "#999999"
    property color activeColor: "#FF8C00"

    property color myColor: "#999999"
    property color notMyColor: "#0078D3"

    property var authorUsername: "System";
    property var authorDisplayName: "System"

    property int childrenHeightOffset:0

    property var scroller:null

    function addMessage(message) {
        qmodel.append(message);
    }

    function scrollToBottom() {
        if(scroller !== null) {
            scroller.scrollToBottom();
        }
    }

    id:qitem

    height: skypeText1.height+childrenHeightOffset

    onParentChanged: {
        width = Qt.binding(function() { return parent.width })
    }

    ColumnLayout {
        id: columnLayout1
        spacing: 0
        anchors.fill: parent

        SkypeLabelE {
            id: skypeText1
            text: "Drew Parker"
            bold: false
            size: 12
            Layout.fillWidth: true

            states:[
                State {
                    name: "DEFAULT_ME"
                    when: !isActive && isMe
                    PropertyChanges {target: skypeText1; color: myColor}
                    PropertyChanges {target: skypeText1; bold: true}
                },State {
                    name: "DEFAULT_NOT_ME"
                    when: !isActive && !isMe
                    PropertyChanges {target: skypeText1; color: notMyColor}
                    PropertyChanges {target: skypeText1; bold: true}
                },State {
                    name: "ACTIVE_ME"
                    when: isActive && isMe
                    PropertyChanges {target: skypeText1; color: myColor}
                    PropertyChanges {target: skypeText1; bold: true}
                },State {
                    name: "ACTIVE_NOT_ME"
                    when: isActive && !isMe
                    PropertyChanges {target: skypeText1; color: activeColor}
                    PropertyChanges {target: skypeText1; bold: false}
                }
            ]

        }

        ListView {
            interactive: false
            Layout.fillHeight: true
            Layout.fillWidth: true
            id: rowLayout1
            highlightRangeMode: ListView.NoHighlightRange
            model: ObjectModel {
                id: qmodel
                onChildrenChanged: {
                    var children=qmodel.children
                    if(children.length > 0) {
                        var child = children[children.length-1]
                        child.width=Qt.binding(function() { return qitem.width })
                        childrenHeightOffset+=child.height
                        scrollToBottom()
                    }
                }
            }
        }
    }

}
