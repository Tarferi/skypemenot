import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {

    id:qitem

    height: 24

    property string buttonText:"Button"
    property var clickCallback:function(){}

    property bool hasArrow: false
    property bool hasIcon: false
    property url icon: "/images/status/call.png"
    property int iconWidth: 16
    property int iconHeight: 16

    property bool enabled: true

    property alias hoverer: qma
    property color defaultColor: "#7fba00"
    property color disabledColor: "#CDE49B"
    property alias hoverColor: qma.hoverColor
    property alias clickedColor: qma.clickedColor

    property int borderWidth: 0
    property color borderColor: defaultColor

    property int _margin: 24

    width: rectangle1.width

    Rectangle {
        id: rectangle1
        color: defaultColor
        radius: 12
        height: parent.height
        width: rowLayout1.width+_margin
        border.color: borderColor
        border.width: borderWidth

        states: [
            State {
                name: "ENABLED"
                when: qitem.enabled
                PropertyChanges {target: rectangle1; color: defaultColor}
            },State {
                name: "DISABLED"
                when: !qitem.enabled
                PropertyChanges {target: rectangle1; color: disabledColor}
            }

        ]

        RowLayout {
            id: rowLayout1
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height


            Image {
                id: theIcon
                anchors.verticalCenter: parent.verticalCenter
                width: hasIcon?iconWidth:0
                height: iconHeight
                Layout.fillWidth: false
                source: icon
                visible: hasIcon
            }

            SkypeLabel {
                text: buttonText
                verticalAlignment: Text.AlignVCenter
                size: 13
                Layout.fillWidth: true
                Layout.fillHeight: true
                anchors.verticalCenter: parent.verticalCenter
                id: skypeText1
                color: "#ffffff"
                bold: false
                visible: text !== ""
            }
        }

        MouseArea {
            id:qma

            property bool hasMouse:false
            readonly property color defaultColor: qitem.defaultColor
            property color hoverColor: "#72A700"
            property color clickedColor: "#598200"
            property int fadeDuration: 200
            property int fadeInDuration: fadeDuration
            property int fadeOutDuration: fadeDuration
            property var target: rectangle1
            property var clickObjMovement: null


            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                if(qitem.enabled) {
                    hasMouse=true;
                }
            }
            onPressed: {
                if(qitem.enabled) {
                    anim1.stop()
                    anim2.stop();
                    target.color=qma.clickedColor
                    if(clickObjMovement !== null) {
                        clickObjMovement.anchors.topMargin=2
                    }
                }
            }
            onReleased: {
                if(qitem.enabled) {
                    anim1.stop()
                    anim2.stop();
                    target.color=qma.defaultColor
                    if(clickObjMovement !== null) {
                        clickObjMovement.anchors.topMargin=2
                    }
                }
            }

            onExited: {
                if(qitem.enabled) {
                    hasMouse=false
                }
            }
            onClicked: {
                if(qitem.enabled) {
                    clickCallback();
                }
            }
            PropertyAnimation {
                id:anim1
                target: qma.target
                property: "color"
                duration:qma.fadeInDuration
                to: qma.hoverColor
            }
            PropertyAnimation {
                id:anim2
                target: qma.target
                property: "color"
                duration: qma.fadeOutDuration
                to: qma.defaultColor
            }
            onHasMouseChanged: {
                if(qitem.enabled) {
                    if(hasMouse) {
                        anim2.stop()
                        anim1.start()
                    } else {
                        anim1.stop()
                        anim2.start()
                    }
                }
            }
        }


    }

}
