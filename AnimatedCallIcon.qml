import QtQuick 2.0
import QtQuick 2.7

Item {
    width: 16
    height: 16



    function animate() {
        rotator1.restart();
    }

    function reset() {
        rotator1.running=false
    }

    property var finishCallback;
    finishCallback: function(){}

    function setSelected(sel) {
        if(sel) {
            image1.source="images/status/call_sel.png"
        } else {
            image1.source="images/status/call.png"
        }
    }

    Rectangle {
        id: rectangle1
        color: "#00000000"
        anchors.fill: parent

        Image {
            id: image1
            x:0
            y:0
            width:16
            height:16
            source: "images/status/call.png"
            SequentialAnimation {
                running: false

                onRunningChanged: {
                    if(!running) {
                        rotator1.stop();
                        image1.rotation=0
                        image1.y=0
                        finishCallback();
                    }
                }

                id: rotator1
                RotationAnimator {
                    target: image1
                    from: 0;
                    to: 135;
                    duration: 400
                    direction: RotationAnimator.Clockwise
                }
                NumberAnimation { target: image1; property: "y"; to: 5; duration: 200 }
                NumberAnimation { target: image1; property: "y"; to: 2; duration: 120 }
                NumberAnimation { target: image1; property: "y"; to: 4; duration: 100 }
                alwaysRunToEnd: false
                PauseAnimation { duration: 8000 }
            }
        }
    }
}
