import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: item1
    width: 120
    height: 30
    state: "NORMAL"

    signal toolBoxSelfClicked()

    property string myName;
    myName:"Drew Parker"

    property alias myStatus: statusIconWithArrow1.status

    function setName(name) {
        myName=name;
    }

    function setStatus(status) {
        myStatus=status;
        toolBoxOnlineMenu1.setDisabledValue(status)
    }

    function setSelftSelected(selected) {
        if(selected) {
            rect5.state="CLICKED"
        } else {
            rect5.state="NORMAL"
        }
    }

    objectName: "obj_toolbox_me"

    property string iconFadeColor: "#1A000000"


    ToolBoxOnlineMenu {
        id: toolBoxOnlineMenu1
    }

    Rectangle {
        state: "NORMAL"
        states: [
            State {
                name: "NORMAL"
                PropertyChanges {target: rect5; color: "transparent"}
                PropertyChanges {target: username; color: "#000000"}
                PropertyChanges {target: statusIconWithArrow1; selected: false}
            },
            State {
                name: "CLICKED"
                PropertyChanges {target: rect5; color: "#0095CC"}
                PropertyChanges {target: username; color: "#ffffff"}
                PropertyChanges {target: statusIconWithArrow1; selected: true}
            }
        ]

        id: rect5
        anchors.fill: parent
        color: "transparent"

        RowLayout {
            id: rowLayout1
            anchors.rightMargin: 5
            anchors.leftMargin: 2
            anchors.fill: parent

            StatusIconWithArrow {
                z:3
                id: statusIconWithArrow1
                width: 35
                Layout.fillHeight: false
                clickCallback: function() {
                    toolBoxOnlineMenu1.popup()
                }
            }

            SkypeLabel {
                id: username
                text: myName
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillHeight: false
                Layout.fillWidth: true

            }
        }
    }

    MouseArea {
        z:-1
        anchors.fill: rect5
        hoverEnabled: true
        onEntered: {
            if(rect5.state == "NORMAL") {
                animateColorIn2.stop();animateColorOut2.start();
                animateColorOut2.stop();animateColorIn2.start();
            }
        }
        onExited:{
            if(rect5.state=="NORMAL") {
                animateColorOut2.stop();animateColorIn2.start();
                animateColorIn2.stop();animateColorOut2.start();
            }
        }
        onClicked:{
            if(rect5.state=="NORMAL") {
                rect5.state="CLICKED"
                rect5.forceActiveFocus()
                toolBoxSelfClicked()
            }
        }
    }

    PropertyAnimation {id: animateColorIn2; target: rect5; properties: "color"; to: iconFadeColor; duration: 250}
    PropertyAnimation {id: animateColorOut2; target: rect5; properties: "color"; to: "transparent"; duration: 250}


}
