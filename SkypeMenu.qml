import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Menu {
    id: qmenu

    function popup() {
        open();
    }

    property int borderWidth: 1;
    property color borderColor: "#E2E2E2";

    property int itemBorderWidth: 2;
    property color itemBorderColor: "#22628F";
    property color itemSelectedColor: "#58A5D6";
    property int realWidth: 130

    property bool hasMouse: false

    leftMargin: 0
    rightMargin: 0

    padding: 2

    width: realWidth
    implicitWidth: realWidth
    contentWidth: realWidth

    onHasMouseChanged: {/*
        if(!hasMouse) {
            for(var i=0;i<children.length;i++) {
                var child=children[i]
                console.log(child);
                if(child.background.hasMouse) {
                    console.log("Mosue transfered");
                    hasMouse=true
                    return;
                }
            }
        }*/

        //console.log("Has mouse: "+hasMouse);
    }

    background: Rectangle {
        anchors.fill:parent
        border.width: borderWidth
        border.color: borderColor

        DropShadow {
            cached: true
            anchors.fill: parent
            horizontalOffset: 3
            verticalOffset: 3
            radius: 6.0
            samples: 16
            color: "#80000000"
            smooth: true
            source: parent
       }
       MouseArea {
           anchors.fill: parent
           hoverEnabled: true
           onEntered:  {
               hasMouse=true;
           }
           onExited: {
               hasMouse=false
           }
       }
    }
}

