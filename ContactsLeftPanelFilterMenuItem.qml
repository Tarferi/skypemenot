import QtQuick 2.0

SkypeMenuItem {

    checkable: true
    checked: false
    enabled: true


    hasOwnIcon: false

    textOffset: 5
    checkOffset: 5

    itemFontSize: 11;
    itemHeight: 20

    defaultTextColor: "#000000"
    hoverTextColor: "#ffffff"
    clickedTextColor: hoverTextColor

    defaultBackground: "transparent"
    hoverBackground: "#5A99D7"
    clickedBackground: hoverBackground

    defaultBorderWidth: 2
    hoverBorderWidth: 2
    clickedBorderWidth: hoverBorderWidth

    defaultBorderColor: "transparent"
    hoverBorderColor: "#22628F"
    clickedBorderColor: hoverBorderColor
}
