import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    id: item1
    x: 0
    width: 300
    height: 32

    property var conversationName:"Username"

    property alias conversationIcon:statusIcon1.vImage;

    StatusIcon {
        anchors.verticalCenter: parent.verticalCenter
        id: statusIcon1
        x: 8
        y: 9
        width: 16
        height: 16
    }

    SkypeLabel {
        id: skypeText1
        anchors.verticalCenter: parent.verticalCenter
        x: 31
        y: 13
        text: conversationName
    }

}
