import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {

    property int selectedIndex;
    width: 550
    selectedIndex:-1

    property alias rightPanelParent: contactsLeftPanelHistory1.rightPanelParent

    Component.onCompleted: {
        setSelectedTab(0);
    }

    function setSelectedTab(index) {
        var i=index
        if(i !== selectedIndex) {
            selectedIndex=i
            if(i === 0) {
                rectangle3.state="NORMAL"
                rectangle2.state="SELECTED"
                rectangle7.visible=true
                rectangle9.visible=false
            } else if(i === 1) {
                rectangle2.state="NORMAL"
                rectangle3.state="SELECTED"
                rectangle7.visible=false
                rectangle9.visible=true
            }
        }
    }

    Rectangle {
        id: rectangle4
        color: "#00000000"
        border.color: "#d9d9d9"
        anchors.fill: parent

        ColumnLayout {
            id: columnLayout1
            anchors.rightMargin: 1
            anchors.leftMargin: 1
            anchors.bottomMargin: 1
            anchors.topMargin: 1
            anchors.fill: parent
            spacing: 0


            RowLayout {
                id: rowLayout1
                Layout.minimumHeight: 36
                Layout.maximumHeight: 36
                Layout.fillWidth: true
                spacing: 0

                Rectangle {
                    id: rectangle2
                    color: "#00000000"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    state:"SELECTED"
                    states: [
                        State{
                            name:"SELECTED"
                            PropertyChanges {target: rectangle2; color: "#ffffff"}
                            PropertyChanges {target: contactsLeftPanelContacts1; visible: true}
                        },
                        State {
                            name:"NORMAL"
                            PropertyChanges {target: rectangle2; color: "#E5E5E5"}
                            PropertyChanges {target: contactsLeftPanelContacts1; visible: false}
                        },
                        State {
                            name:"PRESELECTED"
                            PropertyChanges {target: rectangle2; color: "#ffffff"}
                        }
                    ]
                    MouseArea{
                        anchors.fill: rectangle2
                        onPressed: {
                            if(selectedIndex != 0) {
                                rectangle2.state="PRESELECTED";
                            }
                        }
                        onReleased: {
                            if(selectedIndex != 0) {
                                rectangle2.state="NORMAL";
                            }
                        }

                        onClicked: {
                            setSelectedTab(0);
                            //forceActiveFocus();
                        }
                    }

                    ColumnLayout {
                        id: columnLayout3
                        spacing: 0
                        anchors.fill: parent

                        Rectangle {
                            id: rectangle8
                            width: 200
                            height: 200
                            color: "#00000000"
                            Layout.fillHeight: true
                            Layout.fillWidth: true

                            SkypeLabel {
                                id: text1
                                x: 0
                                y: 0
                                text: qsTr("Kontakty")
                                verticalAlignment: Text.AlignVCenter
                                anchors.fill: parent
                                horizontalAlignment: Text.AlignHCenter
                            }
                        }

                        Rectangle {
                            id: rectangle9
                            color: "#d9d9d9"
                            Layout.fillWidth: true
                            Layout.maximumHeight: 1
                            Layout.preferredHeight: 1
                            visible:false
                        }

                    }
                }


                Rectangle {
                    id: rectangle5
                    width: 1
                    color: "#D9D9D9"
                    Layout.fillHeight: true
                    Layout.maximumWidth: 1
                    Layout.preferredWidth: 1
                }

                Rectangle {
                    id: rectangle3
                    color: "#00000000"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    state:"NORMAL"
                    states: [
                        State{
                            name:"SELECTED"
                            PropertyChanges {target: rectangle3; color: "#ffffff"}
                            PropertyChanges {target: contactsLeftPanelHistory1; visible: true}
                        },
                        State {
                            name:"NORMAL"
                            PropertyChanges {target: rectangle3; color: "#E5E5E5"}
                            PropertyChanges {target: contactsLeftPanelHistory1; visible: false}
                        },
                        State {
                            name:"PRESELECTED"
                            PropertyChanges {target: rectangle3; color: "#ffffff"}
                        }

                    ]
                    MouseArea{
                        anchors.fill: rectangle3
                        onPressed: {
                            if(selectedIndex != 1) {
                                rectangle3.state="PRESELECTED";
                            }
                        }
                        onReleased: {
                            if(selectedIndex != 1) {
                                rectangle3.state="NORMAL";
                            }
                        }

                        onClicked: {
                            setSelectedTab(1);
                            //forceActiveFocus();
                        }
                    }

                    ColumnLayout {
                        id: columnLayout2
                        spacing: 0
                        anchors.fill: parent

                        Rectangle {
                            id: rectangle6
                            width: 200
                            height: 200
                            color: "#00000000"
                            Layout.fillHeight: true
                            Layout.fillWidth: true

                            SkypeLabel {
                                id: text2
                                x: 0
                                y: 0
                                text: qsTr("Historie")
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                anchors.fill: parent
                            }

                            Rectangle {
                                id: unreadrect
                                Layout.minimumWidth: 14
                                height: 14
                                radius: 7
                                color:"#ff8c00"
                                border.color: "#ff8c00"
                                width: unreadtext.width+10
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: parent.right
                                anchors.rightMargin: 10
                                SkypeLabel {
                                    id:unreadtext
                                    text: contactsLeftPanelHistory1.unreadCount
                                    color: "#ffffff"
                                    anchors.centerIn: parent
                                    bold: true
                                    size: 11
                                    states: [
                                        State {
                                            name: "NORMAL"
                                            when: contactsLeftPanelHistory1.unreadCount != 0
                                            PropertyChanges {target: unreadrect; visible: true}
                                        }, State {
                                            name: "HIDDEN"
                                            when: contactsLeftPanelHistory1.unreadCount == 0
                                            PropertyChanges {target: unreadrect; visible: false}
                                        }
                                    ]
                                }
                            }
                        }

                        Rectangle {
                            id: rectangle7
                            color: "#d9d9d9"
                            Layout.fillWidth: true
                            Layout.maximumHeight: 1
                            Layout.preferredHeight: 1
                            visible:false
                        }
                    }
                }

            }

            Rectangle {
                id: rectangle1
                color: "#00000000"
                Layout.fillHeight: true
                Layout.fillWidth: true

                ContactsLeftPanelContacts {
                    id: contactsLeftPanelContacts1
                    visible: false
                    anchors.fill: parent
                }

                ContactsLeftPanelHistory {
                    id: contactsLeftPanelHistory1
                    visible: false
                    anchors.fill: parent
                }
            }
        }
    }

}
