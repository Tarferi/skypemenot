import QtQuick 2.4

Item {
    width: 20
    height: 20

    property string vImage
    vImage:"offline"

    Image {
        id: image1
        source: "images/status/"+vImage+".png"
        anchors.fill: parent
    }

    states: [
        State {
            name: "REGULAR"
            when: vImage != "connecting"
            PropertyChanges { target: image1; rotation: 0 }
            PropertyChanges { target: rotT; running: false }
        }, State {
            name: "SPINNING"
            when: vImage == "connecting"
            PropertyChanges { target: image1; rotation: 0 }
            PropertyChanges { target: rotT; running: true }
        }
    ]

    RotationAnimation {
        id: rotT
        target: image1
        from: 0;
        to: 360;
        duration: 750;
        direction: RotationAnimation.Clockwise;
        loops: Animation.Infinite;
        running: false
    }

}
