
#include <skypeuilib.h>
#include <skypeuilibh.h>
#include "mainwindow.h"

using namespace SkypeUILib;

using std::cout;

void wait(unsigned int seconds) {
    std::this_thread::sleep_for(std::chrono::seconds(seconds));
}

void callback(MainWindow* w) {
    wait(1);
    ContactManager* m=w->getContactsManager();
    Contact* taferi=m->addContact("skype","tav.taferi");
    Contact* encorn=m->addContact("skype","tav.taoge");

    taferi->setStatus(ONLINE);
    encorn->setStatus(OFFLINE);
    wait(999999);
}

int main() {
    MainWindow::create(callback);
    return 0;
}
