/****************************************************************************
** Meta object code from reading C++ file 'toolbox.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../toolbox.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'toolbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SkypeUILib__ToolBox_t {
    QByteArrayData data[13];
    char stringdata0[184];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SkypeUILib__ToolBox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SkypeUILib__ToolBox_t qt_meta_stringdata_SkypeUILib__ToolBox = {
    {
QT_MOC_LITERAL(0, 0, 19), // "SkypeUILib::ToolBox"
QT_MOC_LITERAL(1, 20, 11), // "homeClicked"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 11), // "callClicked"
QT_MOC_LITERAL(4, 45, 14), // "friendsClicked"
QT_MOC_LITERAL(5, 60, 10), // "addClicked"
QT_MOC_LITERAL(6, 71, 14), // "profileClicked"
QT_MOC_LITERAL(7, 86, 15), // "onlineRequested"
QT_MOC_LITERAL(8, 102, 13), // "awayRequested"
QT_MOC_LITERAL(9, 116, 13), // "busyRequested"
QT_MOC_LITERAL(10, 130, 18), // "invisibleRequested"
QT_MOC_LITERAL(11, 149, 16), // "offlineRequested"
QT_MOC_LITERAL(12, 166, 17) // "redirectRequested"

    },
    "SkypeUILib::ToolBox\0homeClicked\0\0"
    "callClicked\0friendsClicked\0addClicked\0"
    "profileClicked\0onlineRequested\0"
    "awayRequested\0busyRequested\0"
    "invisibleRequested\0offlineRequested\0"
    "redirectRequested"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SkypeUILib__ToolBox[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x0a /* Public */,
       3,    0,   70,    2, 0x0a /* Public */,
       4,    0,   71,    2, 0x0a /* Public */,
       5,    0,   72,    2, 0x0a /* Public */,
       6,    0,   73,    2, 0x0a /* Public */,
       7,    0,   74,    2, 0x0a /* Public */,
       8,    0,   75,    2, 0x0a /* Public */,
       9,    0,   76,    2, 0x0a /* Public */,
      10,    0,   77,    2, 0x0a /* Public */,
      11,    0,   78,    2, 0x0a /* Public */,
      12,    0,   79,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SkypeUILib::ToolBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ToolBox *_t = static_cast<ToolBox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->homeClicked(); break;
        case 1: _t->callClicked(); break;
        case 2: _t->friendsClicked(); break;
        case 3: _t->addClicked(); break;
        case 4: _t->profileClicked(); break;
        case 5: _t->onlineRequested(); break;
        case 6: _t->awayRequested(); break;
        case 7: _t->busyRequested(); break;
        case 8: _t->invisibleRequested(); break;
        case 9: _t->offlineRequested(); break;
        case 10: _t->redirectRequested(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject SkypeUILib::ToolBox::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SkypeUILib__ToolBox.data,
      qt_meta_data_SkypeUILib__ToolBox,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SkypeUILib::ToolBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SkypeUILib::ToolBox::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SkypeUILib__ToolBox.stringdata0))
        return static_cast<void*>(const_cast< ToolBox*>(this));
    return QObject::qt_metacast(_clname);
}

int SkypeUILib::ToolBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
