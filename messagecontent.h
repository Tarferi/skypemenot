#ifndef MESSAGECONTENT_H
#define MESSAGECONTENT_H

#include "skypeuilib.h"

namespace SkypeUILib {

    class MessageContent {

    public:
        MessageContent() {}

        ~MessageContent() {
            while(!data.empty()) {
                MessageContentData* el=data.top();
                data.pop();
                delete el;
            }
        }

        void addContentData(MessageContentData* data) {
            this->data.push(data);
        }

        std::stack<MessageContentData*> getData() {
            return data;
        }

    private:
        std::stack<MessageContentData*> data;
    };

}

#endif // MESSAGECONTENT_H
