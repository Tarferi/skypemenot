import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick 2.7
import QtQml 2.2
import QtQml.Models 2.2

Item {

    id: qitem;
    height:qdiff+qcol.height

    property alias qcoll: qcol

    property bool collapsed: false;
    property date date: new Date();

    property string _date:"";

    onDateChanged: {
        _date=transformDate(date);
        updateText();
    }

    property int qdiff: 50;

    function moveToTop(contact) {
        var children=qmodel.children;
        var length=children.length;
        if(length > 0) {
            if (children[0] !== contact) {
                for(var i=0;i<length;i++) {
                    var child=children[i];
                    if(child === contact) {
                        qmodel.move(i,0);
                        return;
                    }
                }
                qmodel.insert(0,contact);
            }
        }
    }

    function resortMe(contact) {
        var children=qmodel.children;
        var item=contact.listItem;
        var length=children.length;
        var found=false;
        var foundIndex=0;
        var foundMe=false;
        var myIndex=0;
        var myTime=contact.lastAction.getTime();
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child === item) {
                foundMe=true;
                myIndex=i;
                if(foundIndex) {
                    break;
                }
            } else if(child.item.lastAction.getTime() < myTime){
                found=true;
                foundIndex=i;
                if(foundMe) {
                    break;
                }
            }
        }
        if(foundMe && foundIndex) {
            qmodel.move(myIndex,foundIndex);
        }
    }

    function updateText() {
        if(collapsed) {
            skypeText1.text=_date+" ("+qmodel.count+")"
            qitem.height=qdiff;
        } else {
            qitem.height=qdiff+qcol.height;
            skypeText1.text=_date;
        }
    }

    function stillBelongs(contact) {
        var lastAtion=contact.lastAction;
    }

    function moveHere(contact) {
        contact.category=qitem;
        var item=contact.listItem;
        var children=qmodel.children;
        var length=children.length;
        var myTime=contact.lastAction.getTime();
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child.item.lastAction.getTime() < myTime) {
                qmodel.insert(i,item);
                return;
            }
        }
        qmodel.append(item);
    }

    function deleteFrom(item) {
        var children=qmodel.children;
        var length=children.length;
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child === item) {
                qmodel.remove(i);
                item.destroy();
                return;
            }
        }
        item.destroy();
        console.error("Child not found while removing");
    }


    property var historian;

    function update() {
        if(qcol.children.length === 0) {
            historian.deleteCat(this);
            destroy();
        }
    }

    property var locale: Qt.locale()

    function getDateFormat(date) {
        return date.toLocaleDateString(locatem,"dd.MM.yyyy");
    }

    property int day:24*60*60*1000;

    function transformDate(date) {
        var now=new Date().getTime();
        var then=date.getTime();
        var diff=now-then;
        if(diff < day) {
            return "Dnes";
        } else if(diff < day+day) {
            return "Včera";
        } else {
            return getDateFormat(date);
        }
    }

    SkypeLabel {
        id: skypeText1
        x: 10
        y: 5
        text: "Dnes"

        MouseArea {
            anchors.fill:parent

            onDoubleClicked: {
                collapsed=!collapsed;
                qcol.visible=!collapsed;
               updateText();
            }
        }
    }

    onWidthChanged: {
        rectangle1.width=width-10;
        console.log("Setting width to "+(width-10));
    }


    Rectangle {
        id: rectangle1
        y: 27
        x: 10
        height: 1
        Layout.minimumHeight: 1
        Layout.maximumHeight: 1
        Layout.preferredHeight: 1
        color: "#D9D9D9"
    }

    ObjectModel {
        id: qmodel;

        property var lastChanger:null;

        onCountChanged: {
            qcol.height=count*80;
            if(count > 0) {
                if(lastChanger!=null) {
                    lastChanger.widthChangeCallback=function(){};
                }
                children[0].item.widthChangeCallback=function(width) {
                    rectangle1.width=width-20
                }
                rectangle1.width=children[0].item.width-20
            }
            updateText();
        }
    }

    ListView {
        model: qmodel
        id: qcol;
        y: 30
        width: parent.width;
        orientation: Qt.Vertical
        spacing: 1
    }

}
