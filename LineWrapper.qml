import QtQuick 2.0

Rectangle {

    id:qrect;

    property var item;

    property var root;

    function addComponent(cmp) {
        item=cmp;
        cmp.parent=qrect;
        qrect.width=cmp.width;
        qrect.height=cmp.height;
    }

}
