import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4


Item {

    id:qitem

    signal messageSent(string message);

    signal emojisClicked();

    property var replacer:null

    function _fileNameToShortForm2(file) {
        var imger =  /.*\/(\w+)\.\w+/gi;
        file=file.replace(imger,"$1");
        return replacer === null?"[16: IT DIDN'T REPLACE LOL]":replacer.getShortByFile(file);
    }

    function _fileNameToShortForm(file){
        var imger = /<img *src="([^"]*)" *[^>]*\/ *>/gi;
        file=file.replace(imger,"$1");
        if(file.length > 0) {
            return _fileNameToShortForm2(file);
        } else {
            console.error("Sending message image replacement failed")
            return "[26: IT DIDN'T WORK LOL]"
        }
    }

    function convertMessage(msg) {
        var newliner = /<br \/>/gi;
        msg=msg.replace(newliner,"\n");

        var styler = /<style [^>]*>[^>]*<\/style>/gi;
        msg=msg.replace(styler,"");

        var imger = /<img *src="([^"]*)" *[^>]*\/ *>/gi;
        msg=msg.replace(imger,function(a) {return _fileNameToShortForm(a)});

        var tager = /<[^>]*>/gi;
        msg=msg.replace(tager,"").trim();

        msg=msg.replace("&quot;","\"");
        msg=msg.replace("&apos;","'");
        msg=msg.replace("&gt;",">");
        msg=msg.replace("&lt;","<");
        msg=msg.replace("&amp;","&");

        return msg;
    }

    function recalcWidth() {
        var w1=rowLayout1.width
        var w2=edit.contentWidth
        qrect.width=w1>w2?w1:w2
    }

    function recalcHeight() {
        var h1=rowLayout1.height
        var h2=edit.contentHeight
        qrect.height=h1>h2?h1:h2
    }

    function appendImage(file, shortForm) {
        edit.insert(edit.cursorPosition,"<img width=20 height=20 src=\""+file+"\" title=\""+shortForm+"\" />");
    }

    RowLayout {
        id: rowLayout1
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        anchors.fill: parent


        onWidthChanged: {
            recalcWidth();
        }
        onHeightChanged: {
            recalcHeight();
        }

        Flickable {
            Layout.fillHeight: true
            Layout.fillWidth: true

            id: flick

            contentWidth: edit.paintedWidth
            contentHeight: edit.paintedHeight
            clip: true

            function ensureVisible(r) {
                if (contentX >= r.x) {
                    contentX = r.x;
                } else if (contentX+width <= r.x+r.width) {
                    contentX = r.x+r.width-width;
                }
                if (contentY >= r.y) {
                    contentY = r.y;
                } else if (contentY+height <= r.y+r.height) {
                    contentY = r.y+r.height-height;
                }
            }

            Rectangle {
                id: qrect
                color:"transparent"
                TextEdit {
                    onContentWidthChanged: {
                        recalcWidth()
                    }
                    onContentHeightChanged: {
                        recalcHeight()
                    }

                    function copy() {
                        console.log("Copying")
                    }


                    id: edit
                    width: parent.width
                    height: parent.height
                    selectByMouse: true
                    selectedTextColor: "#000000"
                    selectionColor: "#9EC3E7"
                    text: "test"
                    onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                    textFormat: Text.RichText
                    FontLoader { id: skypeFont3; source: "fonts/tahoma.ttf" }
                    font.family: skypeFont3.name
                    font.pixelSize: 12
                    font.bold: false;
                    Keys.onPressed: function(event) {
                        if(event.key === Qt.Key_Enter || event.key === Qt.Key_Return) {
                            if(event.modifiers & Qt.ShiftModifier) {
                            } else {
                                event.accepted=true
                                messageSent(convertMessage(edit.text))
                                edit.text=""
                            }
                        }
                    }
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.IBeamCursor;
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        propagateComposedEvents: true

                        onClicked: mouse.accepted = false;
                        onPressed: mouse.accepted = false;
                        onReleased: mouse.accepted = false;
                        onDoubleClicked: mouse.accepted = false;
                        onPositionChanged: mouse.accepted = false;
                        onPressAndHold: mouse.accepted = false;
                    }
                }
            }
        }

        Rectangle {
            id: rectangle1
            width: 20
            color: "#00000000"
            Layout.fillHeight: true

            ColumnLayout {
                id: columnLayout1
                anchors.rightMargin: 5
                anchors.topMargin: 5
                anchors.fill: parent

                Image {
                    id: image1
                    width: 100
                    height: 100
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                    sourceSize.height: 14
                    sourceSize.width: 19
                    source: "images/common/emoji.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            qitem.emojisClicked();
                        }
                    }
                }
            }
        }
    }

}


