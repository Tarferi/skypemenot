import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQml.Models 2.2

Item {

    id:qitem;

    property var categories: {"!":importantCategory};

    property var importantCategory: ContactsLeftPanelHistoryCategoryImportant{}

    property var selected: null;

    property int unreadCount: 0

    property var myUsername:"tav.taferi"
    property var rightPanelParent:null

    function addUnread(count) {
        console.log("Adding: "+count);
        unreadCount+=count;
    }

    property var _qselCB: function(source) {
        if(selected !== source) {
            if(selected !== null) {
                selected.setSelected(false);
            }
            selected=source;
            selected.setSelected(true);
        }
    }

    property var locale: Qt.locale()

    function getOrCreateCat(date) {
        var now=new Date();
        var str=date.toLocaleDateString(date,"dd.MM.yyyy");
        if(categories[str]) {
            return categories[str];
        } else {
            var cat=Qt.createComponent("ContactsLeftPanelHistoryCategory.qml").createObject();
            cat.date=date;
            cat.historian=qitem;
            var children=contactsListModel.children;
            var length=children.length;
            var myTime=date.getTime();
            var found=false;
            for(var i=1;i<length;i++) { // 0 = Important category
                var child=children[i];
                if(child.date.getTime() < myTime) {
                    found=true;
                    contactsListModel.insert(i,cat);
                    break;
                }
            }
            if(!found) {
                contactsListModel.append(cat);
            }
            categories[str]=cat;
            return cat;
        }
    }

    objectName: "ContactsLeftPanelContact_listModel";

    function deleteCat(toRemove) {
        var len=categories.length;
        for(var str in categories) {
            var cat=categories[str];
            if(cat === toRemove) {
                delete categories[str];
                break;
            }
        }
    }

    function addNW(protocol, username) {
        var qcmp=Qt.createComponent("ContactsLeftPanelHistoryContact.qml");
        if(qcmp.status !== Component.Ready) {
            console.error("Failed to create the component");
            if (qcmp.status === Component.Error) {
                console.log("Details:", component.errorString());
            } else {
                console.log("No further details");
            }
        } else {
            var cmp=qcmp.createObject();
            cmp.historian=qitem;
            cmp.protocol=protocol;
            cmp.username=username;
            cmp.displayName="";
            cmp.statusIcon="online";
            cmp.width=Qt.binding(function() {return qscr.width})
            cmp.clickedCB=_qselCB;
        }
        return cmp;
    }

    function changeCat(contact) {
        var currentCategory=contact.category;
        var lastAction=contact.lastAction;
        var newCategory=contact.isCalling?importantCategory:getOrCreateCat(lastAction);
        var originalItem=contact.listItem;
        if(currentCategory !== newCategory) {
            contact.listItem=Qt.createComponent("LineWrapper.qml").createObject();
            contact.listItem.addComponent(contact);
            newCategory.moveHere(contact);
            if(currentCategory !== null) {
                currentCategory.deleteFrom(originalItem);
            }
        }
    }

    ObjectModel {
        id: contactsListModel

        ContactsLeftPanelHistoryCategoryImportant {
            id: importantCategory;
        }
    }

    ScrollView {
        anchors.fill: parent
        ListView {
            id: qscr;
            anchors.fill: parent
            orientation: Qt.Vertical
            spacing: 3
            model: contactsListModel
        }
    }


}
