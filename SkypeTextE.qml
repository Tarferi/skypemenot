import QtQuick 2.0

TextEdit {
    property bool bold;
    bold: false;
    property int size: 12;
    FontLoader { id: skypeFont2; source: "fonts/tahoma.ttf" }
    readOnly: true
    selectByMouse: true
    selectionColor: "#5A99D7"
    font.family: skypeFont2.name
    font.pixelSize: size
    font.bold: bold;
    renderType: Text.NativeRendering
}
