import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Item {

    id:qitem

    signal messageSent(string message);

    function appendMessage(message) {
        qcont.appendMessage(message)
    }

    function appendDate(dater) {
        qcont.appendDate(dater)
    }

    SplitView {
        id: splitView1
        anchors.fill: parent
        orientation: Qt.Vertical

        Rectangle {
            id: chatRect
            Layout.fillHeight: true
            color: "transparent"
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            RightPanelChatContents {
                id:qcont
                anchors.fill: parent
            }
        }

        Rectangle {
            id: inputRect
            width: parent.width
            Layout.minimumHeight: 38
            Layout.maximumHeight: parent.height/2
            height: 87
            RightPanelChatInput{
                id: qinput
                anchors.fill: parent
                replacer: emojij
                onMessageSent: function(msg) {qitem.messageSent(msg);}
                onEmojisClicked: {
                    emojij.focus=true
                }
            }
            color: "transparent"

            EmojiPanel {
                id:emojij
                anchors.right: parent.right
                y:-height
                callbackInserter: function(file, textForm) {
                    qinput.appendImage("/images/emojis/"+file+".gif",textForm);
                    focus=false
                    qinput.focus=true
                }
                visible: focus
            }
        }
    }
}
