#ifndef CONTACTGROUP_H
#define CONTACTGROUP_H

#include "skypeuilib.h"

namespace SkypeUILib {

    class ContactGroup {

    public:
        ContactGroup();

        void setGroupName(QString newName);

        void startCall();

        void endCall();

        void appendMessage(Message m);

    };

}

#endif // CONTACTGROUP_H
