import QtQuick 2.7
import QtQuick 2.0

Item {
    property int realHeight
    realHeight:1
    property variant color
    color: "#ff0000"

    Canvas {
        id: mycanvas
        anchors.fill: parent
        onPaint: {
            var ctx = getContext("2d");
            ctx.lineWidth = realHeight
            ctx.strokeStyle = color
            ctx.moveTo(0,0)
            ctx.lineTo(width, 0)
            ctx.stroke()
        }
    }
}
