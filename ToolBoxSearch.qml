import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4

Item {
    width: 160
    height: 25

    signal s_ToolBoxSearchCancelPressed();
    signal s_ToolBoxSearchPressed(string search);
    signal s_ToolBoxSearchEnterPressed(string search);

    function setSearchContent(search) {
        searchInput.text=search;
        _showhidedisicon();
    }

    function _showhidedisicon() {
        cancelIcon.visible=searchInput.text.length>0
    }


    Rectangle {
        id: rectangle1
        color: "#ffffff"
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        border.width: 1
        border.color: "#d9d9d9"
        anchors.fill: parent

        Rectangle {
            id: rectangle2
            color: "#ffffff"
            anchors.bottomMargin: 2
            anchors.topMargin: 1
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.fill: parent

            RowLayout {
                id: rowLayout1
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.fill: parent

                Image {
                    id: searchIcon
                    width: 13
                    height: 13
                    source: "images/common/search.png"
                    MouseArea {
                        id: marea
                        anchors.fill: parent
                        onClicked: {
                            s_ToolBoxSearchPressed(searchInput.text)
                        }
                    }
                }
                TextField {
                    id: searchInput
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    placeholderText: "Hledat"
                    style: TextFieldStyle {
                        textColor: "black"
                        placeholderTextColor: "#d9d9d9"
                        background: Rectangle {
                            border.width: 0
                        }
                    }
                    Keys.onEnterPressed: {
                        s_ToolBoxSearchEnterPressed(searchInput.text)
                    }
                    onTextChanged: {
                        _showhidedisicon();
                    }
                    onFocusChanged: {
                        if(focus) {
                            rectangle1.border.color="#000000"
                        } else {
                            rectangle1.border.color="#d9d9d9"
                        }
                    }
                }

                Image {
                    id: cancelIcon
                    width: 14
                    height: 14
                    source: "images/common/cancel.png"
                    visible: false
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            s_ToolBoxSearchCancelPressed()
                        }
                    }
                }

            }
        }
    }

}
