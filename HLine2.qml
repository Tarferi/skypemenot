import QtQuick 2.0

Item {

    id:qitem

    height:realHeight
    width:parent.width

    property color realBackground: "#E2E2E2"
    property int realHeight: 6
    property int lineHeight: 1

    Rectangle {
        anchors.fill: parent
        color: "transparent"
        Rectangle {
            width: parent.width
            height: lineHeight
            color: realBackground
            y: (realHeight)/2
            x: 0
        }
    }
}
