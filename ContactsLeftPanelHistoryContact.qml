import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    height: 24

    id:itm1

    signal callEndClicked();

    signal callClicked();

    property int _unreadCount: 0
    property int unreadCount: 0

    property var widthChangeCallback:function(){}

    onWidthChanged: {
        widthChangeCallback(width);
    }

    signal qWidthChanged(int width);

    onUnreadCountChanged: {
        if(unreadCount == 0) {
            if(_unreadCount != 0) {
                historian.addUnread(-1);
            }
        } else {
            if(_unreadCount == 0) {
                historian.addUnread(1);
            }
        }
        if(unreadCount == 0) {
            unreadanim.stop()
        } else {
            unreadanim.stop()
            unreadanim.start();
        }
        _unreadCount=unreadCount
    }

    property date lastAction: new Date();
    property var category: null;
    property var historian: null;


    property var listItem:null;


    onLastActionChanged: {
        if(historian!=null){
            if(category != null) {
                if(!category.stillBelongs(this)) {
                    historian.changeCat(this);
                } else {
                    category.resortMe(this);
                }
            } else {
                historian.changeCat(this);
            }
        }
    }

    property string protocol
    property string displayName;
    property string username
    property string statusIcon
    property bool isCalling:false

    property bool _hasCallingIcon: false;


    property var chatObj: null;

    function setIsCalling(call) {
        isCalling=call;
    }

    Component.onCompleted: {
        update();
        _setCall(isCalling,true);
        var cobj=Qt.createComponent("RightPanel.qml");
        chatObj=cobj.createObject();
        chatObj.visible=false
        chatObj.conv=itm1
        chatObj.conversationIcon=statusIcon
        chatObj.conversationName=_displayedName
    }

    onHistorianChanged: {
        if(historian !== null) {
            chatObj.myUsername=historian.myUsername
            chatObj.parent=historian.rightPanelParent
        }
    }

    on_HasCallingIconChanged: {
        setCall(isCalling);
        if(category != null) {
            if(!category.stillBelongs(this)) {
                historian.changeCat(this);
            }
        } else {
            historian.changeCat(this);
        }
    }

    onIsCallingChanged: {
        setCall(isCalling);
        if(isCalling){
            _hasCallingIcon=isCalling;
        }
    }

    username:"Username"
    statusIcon:"online"
    property var _displayedName:""

    signal callStarted();

    property var clickedCB;
    clickedCB: function(source){}

    onStatusIconChanged: {
        chatObj.conversationIcon=statusIcon
    }

    function setSelected(selected) {
        if(selected) {
            chatObj.visible=true
            rectangle1.state="SELECTED";
        } else {
            chatObj.visible=false
            rectangle1.state="NORMAL";
        }
    }

    onUsernameChanged: {
        update();
    }

    onDisplayNameChanged: {
        update();
    }

    function update() {
        var displayedName=username;
        if(displayName!="") {
            displayedName=displayName;
        }
        _displayedName=displayedName;
        if(chatObj !== null) {
            chatObj.conversationName=_displayedName
        }
    }

    function setUnreadMessages(count) {

    }

    function setCall(call) {
        _setCall(call,false);
    }

    function _setCall(call, forced) {
        if(call) {
            animateColorOut7.stop();
            qstatusIconCall.reset();
            callRect.opacity=1;
            qstatusIcon.visible=false
            qstatusIconCall.visible=true
            callRect.visible=true


        } else {
            callRect.visible=false
            if(!forced) {
                animateColorOut7.start();
                qstatusIconCall.animate()
            } else {
                qstatusIconCall.visible=false
                qstatusIcon.visible=true
            }
            callRect.opacity=1;
        }
    }


    Rectangle {
        id: rectangle1
        color: "#00000000"
        anchors.fill: parent

        state: "NORMAL"
        states: [
            State {
                name: "NORMAL"
                PropertyChanges {target: rectangle1; color: "transparent"}
                PropertyChanges {target: skypeText1; color: "#000000"}
            },
            State {
                name: "SELECTED"
                PropertyChanges {target: rectangle1; color: "#0095CC"}
                PropertyChanges {target: skypeText1; color: "#ffffff"}
            }
        ]

        ContactsLeftPanelContactContextMenu {
            id:myContext
            contact:itm1
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                if(mouse.button & Qt.RightButton) {
                    clickedCB(itm1);
                    myContext.repos(mouse.x,mouse.y);
                    myContext.popup();
                } else {
                    clickedCB(itm1);
                }
            }
            acceptedButtons: Qt.LeftButton | Qt.RightButton
        }

        RowLayout {
            id: rowLayout1
            anchors.rightMargin: 4
            spacing: 9
            anchors.leftMargin: 10
            anchors.fill: parent

            Rectangle {
                color: "#00000000"
                Layout.maximumHeight: 16
                Layout.maximumWidth: 16
                Layout.minimumHeight: 16
                Layout.minimumWidth: 16
                Layout.alignment: Qt.AlignLeft | Qt.AlignCenter
                Image {
                    id: qstatusIcon
                    Layout.maximumHeight: 16
                    Layout.maximumWidth: 16
                    source: "images/status/"+statusIcon+".png"
                    visible:false
                }
                AnimatedCallIcon {
                    id: qstatusIconCall
                    Layout.maximumHeight: 16
                    Layout.maximumWidth: 16
                    visible:true
                    finishCallback: function() {
                        qstatusIconCall.visible=false
                        qstatusIcon.visible=true
                        _hasCallingIcon=false
                    }
                }
            }

            SkypeText {
                id: skypeText1
                text: _displayedName;
                Layout.fillWidth: true
            }

            Rectangle {
                id: unreadrect
                Layout.minimumWidth: 14
                height: 14
                radius: 7
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                color:"#ffffff"
                border.color: "#ff8c00"
                width: unreadtext.width+7

                SkypeLabel {
                    id:unreadtext
                    text: unreadCount
                    color: "#ff8c00"
                    anchors.centerIn: parent
                    bold: true
                    size: 11
                    states: [
                        State {
                            name: "NORMAL"
                            when: unreadCount != 0
                            PropertyChanges {target: unreadrect; visible: true}
                            PropertyChanges {target: skypeText1; bold: true}
                        }, State {
                            name: "HIDDEN"
                            when: unreadCount == 0
                            PropertyChanges {target: unreadrect; visible: false}
                            PropertyChanges {target: skypeText1; bold: false}
                        }
                    ]
                }
                property var nwanimation: SequentialAnimation {
                    id:unreadanim
                    loops: 5
                    PropertyAnimation {
                        duration: 200
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: -4
                    }
                    PropertyAnimation {
                        duration: 200
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: 4
                    }
                    PropertyAnimation {
                        duration: 350
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: -4
                    }
                    PropertyAnimation {
                        duration: 200
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: 4
                    }
                    PropertyAnimation {
                        duration: 350
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: -4
                    }
                    PropertyAnimation {
                        duration: 200
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: 4
                    }
                    PropertyAnimation {
                        duration: 100
                        target: unreadrect
                        property: "Layout.topMargin"
                        to: 0
                    }
                    PropertyAnimation {
                        duration: 2000
                    }
                }

                states: [
                    State {
                        name:"SELECTED"
                        when: rectangle1.state === "SELECTED"
                        PropertyChanges {target: unreadrect; border.color: "transparent"}
                    },
                    State {
                        name:"NORMAL"
                        when: rectangle1.state !== "SELECTED"
                        PropertyChanges {target: unreadrect; border.color: "#ff8c00"}
                    }
                ]
            }

            Rectangle {
                id: callRect
                Layout.maximumHeight: 16
                Layout.maximumWidth: 16
                color: "#00000000"
                Layout.minimumHeight: 16
                Layout.minimumWidth: 16
                Layout.fillWidth: false
                Layout.alignment: Qt.AlignLeft | Qt.AlignCenter



                Image {
                    id: image1
                    width: 16
                    height: 16
                    anchors.fill: parent
                    source: "images/status/call2.png"
                    opacity:1
                }

                Image {
                    id: image2
                    width: 16
                    height: 16
                    anchors.fill: parent
                    source: "images/status/call2_hover.png"
                    opacity:0
                }

                Image {
                    id: image3
                    width: 16
                    height: 16
                    anchors.fill: parent
                    source: "images/status/call2_click.png"
                    opacity:0
                }

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        rectangle1.forceActiveFocus();
                        callEndClicked()
                    }
                    onEntered: {animateColorOut5.stop();animateColorIn5.start();}
                    onExited: {animateColorIn5.stop();animateColorOut5.start();}
                    onPressed: {animateColorIn6.start();}
                }

            }
            PropertyAnimation {id: animateColorIn5; target: image2; properties: "opacity"; to: 1; duration: 250}
            PropertyAnimation {id: animateColorOut5; target: image2; properties: "opacity"; to: 0; duration: 250}

            PropertyAnimation {id: animateColorIn6; target: image3; properties: "opacity"; to: 1; duration: 25}
            PropertyAnimation {id: animateColorOut7; target: callRect; properties: "opacity"; to: 0; duration: 100;}

        }
    }

}
