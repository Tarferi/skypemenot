#ifndef TOOLBOX_H
#define TOOLBOX_H

#include "skypeuilib.h"

namespace SkypeUILib {

    class ToolBox: public QObject {

        Q_OBJECT

    public:
        ToolBox(QObject* window) {
            toolboxObj=window->findChild<QObject*>("obj_toolbox");
            toolboxme=window->findChild<QObject*>("obj_toolbox_me");
            QObject::connect(toolboxObj,SIGNAL(toolBoxOnlineRequested()),this,SLOT(onlineRequested()));
            QObject::connect(toolboxObj,SIGNAL(toolBoxAwayRequested()),this,SLOT(awayRequested()));
            QObject::connect(toolboxObj,SIGNAL(toolBoxBusyRequested()),this,SLOT(busyRequested()));
            QObject::connect(toolboxObj,SIGNAL(toolBoxInvisibleRequested()),this,SLOT(invisibleRequested()));
            QObject::connect(toolboxObj,SIGNAL(toolBoxOfflineRequested()),this,SLOT(offlineRequested()));
            QObject::connect(toolboxObj,SIGNAL(toolBoxRedirectRequested()),this,SLOT(redirectRequested()));


            setMyName("Drew Parker");
            setOnlineStatus(ONLINE);


        }

    public slots:
        void homeClicked();
        void callClicked();
        void friendsClicked();
        void addClicked();
        void profileClicked();

        void onlineRequested() {setOnlineStatus(ONLINE);}
        void awayRequested() {setOnlineStatus(AWAY);}
        void busyRequested() {setOnlineStatus(BUSY);}
        void invisibleRequested() {setOnlineStatus(INVISIBLE);}
        void offlineRequested() {setOnlineStatus(OFFLINE);}
        void redirectRequested();


    public:
        void setMyName(QString myName) {
            QVariant returnedValue;
            QMetaObject::invokeMethod(toolboxme, "setName", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, myName));
        }

        void setOnlineStatus(STATUS statusName) {
            QVariant returnedValue;
            switch(statusName) {
            case ONLINE:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("online")));
            break;
            case AWAY:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("away")));
            break;
            case BUSY:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("busy")));
            break;
            case INVISIBLE:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("offline")));
            break;
            case OFFLINE:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("offline")));
            break;
            case CONNECTING:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("connecting")));
            break;
            default:
                QMetaObject::invokeMethod(toolboxme, "setStatus", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, QString("offline")));
            break;
            }
        }

    private:
        QObject* toolboxObj;
        QObject* toolboxme;

    };

}
#endif // TOOLBOX_H
