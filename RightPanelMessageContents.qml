import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    id: item1

    property bool isEdited: false

    property date date:new Date();

    function _update() {
        time=date.getHours()+":"+date.getMinutes()
    }

    Component.onCompleted: {
        _update();
    }

    onDateChanged: {
        _update();
    }

    property string time: "0:00"
    property bool isNew: false
    property int margin: 5


    height: rowLayout1.height+margin

    property alias text: skypeText3.text

    RowLayout {
        id: rowLayout1
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter

        Column {
            id: column1
            Layout.fillWidth: true
            height: skypeText3.height
            width: skypeText3.width

            SkypeTextE {
                id: skypeText3
                text: "This is the message content"
                size: 11
                horizontalAlignment: Text.AlignLeft
            }
        }

        RowLayout {
            id: columnLayout1
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            Rectangle {
                width: 5
                height: 5
                Layout.minimumWidth: 5
                Rectangle {
                    id: rectangle1
                    width: 5
                    height: 5
                    color: "#FF8E05"
                    visible: isNew
                }
            }

            SkypeTextE {
                id: skypeText2
                color: "#999999"
                height: 20
                text: time
            }

            Rectangle {
                width: 11
                height: 8

                Image {
                    id: image1
                    width: 11
                    height: 8
                    sourceSize.height: 11
                    sourceSize.width: 8
                    source: "images/common/edited.png"
                    visible: isEdited
                }
            }

        }
    }

}
