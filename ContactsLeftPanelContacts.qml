import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQml.Models 2.2
import QtQuick.Controls 1.4

Item {

    property var selected;
    selected: null;
    property var _qselCB;
    _qselCB: function(source) {
        if(selected !== source) {
            if(selected !== null) {
                selected.setSelected(false);
            }
            selected=source;
            selected.setSelected(true);
        }
    }
    id:mItem

    property var catGetter;
    catGetter: function(date) {

    }

    signal qroot();


    function _removeFromList(item, model) {
        var children=model.children;
        var length=children.length;
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child === item) {
                model.remove(i);
                return;
            }
        }
        item.destroy();
    }

    function setCalling(contact, calling) {
        var originalItem=contact.listItem;
        var sourceList;
        var targetList=calling?importantContactsListModel:contactsListModel;
        if(originalItem !== null) { // Movement
            sourceList=originalItem.root;
        } else { // Creation
            sourceList=calling?contactsListModel:importantContactsListModel;
        }
        if(sourceList !== targetList) {
            if(originalItem !== null) {
                _removeFromList(contact.listItem,sourceList);
            }
            contact.listItem=Qt.createComponent("LineWrapper.qml").createObject();
            contact.listItem.addComponent(contact);
            contact.listItem.root=targetList;
            targetList.append(contact.listItem);
            primp.height=contact.height*primp.count;
        }
    }


    ObjectModel {
        id: importantContactsListModel
     }

    ListView {
        id:primp
        x:0
        y:0
        width: parent.width
        height:0
        orientation: Qt.Vertical
        spacing: 1
        model: importantContactsListModel
    }

    Rectangle {
        id: prect4
        anchors.topMargin: primp.height
        anchors.fill: parent
        objectName: "contactsLeftPanelContacts_contactsRow"

        function addNW(protocol, username) {
            var cmp=Qt.createComponent("ContactsLeftPanelContact.qml").createObject();
            cmp.protocol=protocol;
            cmp.username=username;
            cmp.displayName="";
            cmp.description="Hello";
            cmp.statusIcon="online";
            cmp.width=Qt.binding(function() {return qscr.width})
            cmp.clickedCB=_qselCB;
            cmp.contacter=mItem;
            setCalling(cmp,false);
            return cmp;
        }

        ObjectModel {
            id: contactsListModel
         }

        ScrollView {
            anchors.topMargin: 5
            anchors.fill: parent
            ListView {
                onWidthChanged: {
                    qscr.headerItem.width=width;
                }
                id: qscr;
                anchors.fill: parent
                orientation: Qt.Vertical
                spacing: 1
                model: contactsListModel
                header: ContactsLeftPanelContactSorter {
                    onFilterRequest:function(str) {commitFilter(str);}
                }
            }
        }
     }

    function allComparator(item) {
        return true
    }

    function onlineComparator(item) {
        return item.statusIcon !== "offline";
    }

    function skypeComparator(item) {
        return item.protocol="skype";
    }

    function falseComparator(item) {
        return false;
    }

    function commitFilter(protocol) {
        var comparator;
        switch(protocol) {
            case "all":
                comparator=allComparator;
            break;
            case "online":
                comparator=onlineComparator;
            break;
            case "skype":
                comparator=skypeComparator;
            break;
            default:
                comparator=falseComparator;
            break;
        }
        var children=contactsListModel.children;
        var length=children.length;
        for(var i=0;i<length;i++) {
            var child=children[i];
            var item=child.item;
            child.visible=comparator(item);
        }
    }
}
