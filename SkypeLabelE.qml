import QtQuick 2.0

TextEdit {
    FontLoader { id: skypeFont; source: bold?"fonts/segoe_bold.ttf":"fonts/segoe.ttf" }
    font.bold: bold
    font.family: skypeFont.name
    readOnly: true
    selectByMouse: true
    selectionColor: "#5A99D7"
    font.pixelSize: size
    renderType: Text.NativeRendering
    property int size: 14
    property bool bold: false
}
