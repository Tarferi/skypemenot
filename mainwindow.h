#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "skypeuilib.h"

#include "toolbox.h"
#include "contactmanager.h"

using std::cout;

namespace SkypeUILib {

    class QMainThread: public QThread {
        public:
            void run();

            QMainThread::QMainThread(void* window, void (*callback)(MainWindow*), QGuiApplication* app) {
                this->smth=window;
                this->callback=callback;
                this->app=app;
            }

        private:
            void* smth;
            void (*callback)(MainWindow*);
            QGuiApplication* app;

            void end() {
                app->exit(0);
                delete this;
            }
    };


    class MainWindow: public QObject {

    Q_OBJECT

    public:
        static void create( void (*callback)(MainWindow*)) {
            MainWindow* instance=new MainWindow();
            instance->init(callback);
        }

        ToolBox* getToolBox() {
            return toolbox;
        }

        ContactManager* getContactsManager() {
            return contactsManager;
        }

        public slots:
            void uiLoaded() {
                std::cout << "Slot activated\n";
                //thr->start();
            }

    private:

        MainWindow() {
            thr=NULL;
            toolbox=NULL;
        }

        ~MainWindow() {
            if(thr!=NULL) {
                //terminate(thr);
            }
            delete toolbox;
            delete thr;
        }

        void init(void (*callback)(MainWindow*)) {
            int argc=0;
            QGuiApplication app(argc,(char**)"No arguments");
            QGuiApplication* appp=&app;
            QQmlApplicationEngine engine;
            engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
            windowObj=engine.rootObjects().first();
            //QObject::connect(windowObj,SIGNAL(guiLoaded()),this,SLOT(uiLoaded()));
            toolbox=new ToolBox(windowObj);
            contactsManager=new ContactManager(windowObj);
            thr=new QMainThread(this, callback, appp);
            thr->start();
            app.exec();
        }


    private:
        ToolBox* toolbox;
        ContactManager* contactsManager;
        QObject* windowObj;
        QMainThread* thr;
    };
}

#endif // MAINWINDOW_H
