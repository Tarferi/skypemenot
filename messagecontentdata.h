#ifndef MESSAGECONTENTDATA_H
#define MESSAGECONTENTDATA_H

#include "skypeuilib.h"

namespace SkypeUILib {

    class MessageContentData {

    public:
        MessageContentData(QString source){
            component=createObject(source);
        }

        ~MessageContentData() {
            delete component;
        }

        QObject* getObject() {
            return component;
        }

        virtual QObject* createObject(QString source)=0;

    private:
        QObject* component;


    };


    class MessageContentData_TEXT: public MessageContentData {

        QObject* createObject(QString source);

    };

}

#endif // MESSAGECONTENTDATA_H
