import QtQuick 2.0

SkypeMenuItem {

    hasOwnIcon: false
    defaultTextColor: "#000000"
    hoverTextColor: "#ffffff"
    clickedTextColor: hoverTextColor

    defaultBackground: "transparent"
    hoverBackground: "#5A99D7"
    clickedBackground: hoverBackground

    defaultBorderWidth: 0
    hoverBorderWidth: 0
    clickedBorderWidth: hoverBorderWidth

    defaultBorderColor: "transparent"
    hoverBorderColor: "transparent"
    clickedBorderColor: hoverBorderColor

    checkable: false
    itemFontSize: 11

    textOffset: 24
    itemHeight: 20
}
