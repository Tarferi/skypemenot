#ifndef CONTACT_H
#define CONTACT_H

#include "skypeuilib.h"

#include <typeinfo>

namespace SkypeUILib   {

    class Contact: public QObject {

        Q_OBJECT



    public:
        Contact(QObject* parent, QObject* parentHistory, std::string protocol, std::string username) {
            QString qprotocol=QString::fromStdString(protocol);
            QString qusername=QString::fromStdString(username);
            QVariant ret;
            QMetaObject::invokeMethod(parent, "addNW", Qt::BlockingQueuedConnection ,Q_RETURN_ARG(QVariant,ret), Q_ARG(QVariant, qprotocol),Q_ARG(QVariant,qusername));
            contact=ret.value<QObject*>();
            QQmlEngine::setObjectOwnership(contact, QQmlEngine::CppOwnership);
            QMetaObject::invokeMethod(parentHistory, "addNW", Qt::BlockingQueuedConnection ,Q_RETURN_ARG(QVariant,ret), Q_ARG(QVariant, qprotocol),Q_ARG(QVariant,qusername));
            contactHistory=ret.value<QObject*>();
            QQmlEngine::setObjectOwnership(contactHistory, QQmlEngine::CppOwnership);
            QObject::connect(contact,SIGNAL(callEndClicked()),this,SLOT(callEndClicked()),Qt::DirectConnection);
            QObject::connect(contactHistory,SIGNAL(callEndClicked()),this,SLOT(callEndClicked()),Qt::DirectConnection);
            setUsername(qusername);
            setStatus(OFFLINE);
            setCalling(false);
        }

        ~Contact() {
            contact->deleteLater();
            contactHistory->deleteLater();
        }

        void setUsername(QString newName) {
            contact->setProperty("username",newName);
            contactHistory->setProperty("username",newName);
        }

        void setDisplayName(QString newName) {
            contact->setProperty("displayName",newName);
            contactHistory->setProperty("displayName",newName);
        }

        void setStatus(STATUS newStatus) {
            QString str=STATUS2STR(newStatus);
            contact->setProperty("statusIcon",str);
            contactHistory->setProperty("statusIcon",str);
        }

        void setDescription(QString description) {
            contact->setProperty("description", description);
        }

        void setUnreadCount(int count) {
            contactHistory->setProperty("unreadCount",count);
            contact->setProperty("hasUnreadMessages",(bool)(count>0));
        }

        void setCalling(bool calling, bool direct=false) {
            if(direct) {
                QMetaObject::invokeMethod(contact, "setIsCalling", Qt::DirectConnection , Q_ARG(QVariant, calling));
                QMetaObject::invokeMethod(contactHistory, "setIsCalling", Qt::DirectConnection , Q_ARG(QVariant, calling));
            } else {
                QMetaObject::invokeMethod(contact, "setIsCalling", Qt::BlockingQueuedConnection , Q_ARG(QVariant, calling));
                QMetaObject::invokeMethod(contactHistory, "setIsCalling", Qt::BlockingQueuedConnection , Q_ARG(QVariant, calling));
            }
            setLastActionDate();
        }

        void setLastActionDate(QDateTime* date=NULL) {
            if(date==NULL) {
                QDateTime d=QDateTime::currentDateTime();
                contactHistory->setProperty("lastAction", QVariant::fromValue(d));
            } else {
                QDateTime d=*date;
                contactHistory->setProperty("lastAction", QVariant::fromValue(d));
            }
        }

    public slots:
        void callEndClicked() {
            setCalling(false,true);
            setLastActionDate();
        }

     private:
        QObject *contact;
        QObject *contactHistory;
    };

}


#endif // CONTACT_H
