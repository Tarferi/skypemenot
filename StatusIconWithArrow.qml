import QtQuick 2.4
import QtQuick.Layouts 1.3

Item {

    function entered() {
        animateColorOut.stop();animateColorIn.start();
    }
    function exited() {
        animateColorIn.stop();animateColorOut.start();
    }

    property bool selected;
    selected: false

    property var clickCallback
    clickCallback:{}

    property alias status: statusIcon1.vImage;

    id: item1
    width: 35
    height: 23
    Rectangle {
        id: rect1
        width: 35
        height: 20
        radius: 3
        color: "#00000000"
        border.color: "#00000000"
        anchors.fill: parent

        state: "NORMAL"
        states: [
            State {
                name: "NORMAL"
                when: !selected
                PropertyChanges {target: image1; source: "images/common/arrowdown.png"}
            }, State {
                name: "SELECTED"
                when: selected
                PropertyChanges {target: image1; source: "images/common/arrowdown_white.png"}
            }
        ]
        Grid {
            id: grd1
            anchors.fill: parent
            anchors.margins: 4
            columns: 2
            spacing: 5
            verticalItemAlignment: Grid.AlignVCenter

            StatusIcon {
                id: statusIcon1
                vImage: "offline"
                width: 16
                height: 16
            }

            Image {
                id: image1
                x: 0
                y: 0
                width: 7
                height: 4
                source: "images/common/arrowdown.png"

            }
        }

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            hoverEnabled: true
            onEntered: {animateColorIn.start();animateColorOut.stop();}
            onExited: {animateColorIn.stop();animateColorOut.start();}
            onClicked: {
                clickCallback()
            }
        }
        PropertyAnimation {id: animateColorIn; target: rect1; properties: "color"; to: "#2A000000"; duration: 250}
        PropertyAnimation {id: animateColorOut; target: rect1; properties: "color"; to: "transparent"; duration: 250}

    }
}
