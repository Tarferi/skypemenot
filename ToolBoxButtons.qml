import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    width: 160
    height: 36

    signal s_ToolBoxHomePressed();
    signal s_ToolBoxCallPressed();
    signal s_ToolBoxFriendsPressed();
    signal s_ToolBoxAddPressed();

    function toolBoxSetHomeSelected(selected) {
        homeIcon.setSelected(selected);
    }
    function toolBoxSetCallSelected(selected) {
        callIcon.setSelected(selected);
    }
    function toolBoxSetFriendsSelected(selected) {
        friendsIcon.setSelected(selected);
    }
    function toolBoxSetAddSelected(selected) {
        addIcon.setSelected(selected);
    }


    RowLayout {
        id: rowLayout1
        width: 160
        height: 40
        spacing: 0
        anchors.fill: parent

        ToolBoxIcon {
            id: homeIcon
            imageV: "home"
            clickHandler: function(){
                s_ToolBoxHomePressed();
            }
        }

        Rectangle {
            id: tbbrect1
            width: 0
            color: "#00000000"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }


        ToolBoxIcon {
            id: callIcon
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            imageV: "phone"
            clickHandler: function(){
                s_ToolBoxCallPressed();
            }
        }

        Rectangle {
            width: tbbrect1.width
            color: "#00000000"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        ToolBoxIcon {
            id: friendsIcon
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            imageV: "friends"
            clickHandler: function(){
                s_ToolBoxFriendsPressed();
            }
        }
        Rectangle {
            width: tbbrect1.width
            color: "#00000000"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }


        ToolBoxIcon {
            id: addIcon
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            imageV: "add"
            clickHandler: function(){
                s_ToolBoxAddPressed();
            }
        }

    }

}
