#ifndef SKYPEUILIB_H
#define SKYPEUILIB_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQuickItem>
#include <QObject>
#include <qobject.h>
#include "QQmlComponent.h"
#include "QQmlProperty.h"
#include "qqmlengine.h"
#include <iostream>
#include <stack>
#include <qvariant.h>
#include <QThread>
#include <thread>
#include <mutex>
#include <unordered_map>
#include <QtQml>
#include <QDebug>

namespace SkypeUILib {
    class Contact;
    class ToolBox;
    class MainWindow;

    enum STATUS {
        ONLINE=0,
        AWAY,
        BUSY,
        INVISIBLE,
        OFFLINE,
        CONNECTING
    };

    QString STATUS2STR(STATUS status);


    class ContactGroup;
    class Message;
    class MessageContent;
    class MessageContentData;
    class ContactManager;
}

#include "skypeuilibh.h"


#endif // SKYPEUILIB_H
