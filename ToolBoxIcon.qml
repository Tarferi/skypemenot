import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    width: 36
    height: 36
    property string imageV
    imageV: "home"

    property var clickHandler;
    clickHandler:function(){}

    function setSelected(selected) {
        if(selected) {
            rect1.state="CLICKED"
        } else {
            rect1.state="NORMAL"
        }
    }

    Rectangle {
        id: rect1
        color: "#00000000"
        anchors.fill: parent
        property bool clicked
        state: "NORMAL"

        onFocusChanged: {
            if(!focus) {
                rect1.state="NORMAL"
            }
        }

        Image {
            id: image1
            width: 36
            height: 36
            x:0
            y:0

            source: "images/toolbox/"+imageV+".png"

            MouseArea {
                id: marea
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    rect1.forceActiveFocus()
                    rect1.state="CLICKED"
                    clickHandler();
                }
                onEntered: {animateColorIn.start();animateColorOut.stop();}
                onExited: {animateColorIn.stop();animateColorOut.start();}

            }
        }

        PropertyAnimation {id: animateColorIn; target: rect1; properties: "color"; to: "#1A000000"; duration: 250}
        PropertyAnimation {id: animateColorOut; target: rect1; properties: "color"; to: "transparent"; duration: 250}

        states: [
            State {
                name: "NORMAL"
                PropertyChanges {target: image1; source: "images/toolbox/"+imageV+".png"}
            },
            State {
                name: "CLICKED"
                PropertyChanges {target: image1; source: "images/toolbox/"+imageV+"_sel.png"}
            }
        ]
    }
}
