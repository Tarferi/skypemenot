import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    height: 48


    RowLayout {
        id: rowLayout1
        anchors.leftMargin: 10
        spacing: 10
        anchors.fill: parent

        SkypeButton {
            id: skypeButton1
            hasIcon: true
            iconWidth: 16
            iconHeight: 16
            icon: "/images/common/cam_sel.png"
            buttonText: "Videohovor"
            enabled: false
            anchors.verticalCenter: parent.verticalCenter
        }

        SkypeButton {
            id: skypeButton2
            anchors.verticalCenterOffset: 0
            hasIcon: true
            iconWidth: 16
            iconHeight: 16
            icon: "/images/status/call_sel.png"
            buttonText: "Volat"
            anchors.verticalCenter: parent.verticalCenter
        }

        RightPanelWhiteButton {
            anchors.verticalCenterOffset: 0
            hasIcon: true
            iconWidth: 14
            iconHeight: 14
            icon: "/images/common/add.png"
            buttonText: ""
            anchors.verticalCenter: parent.verticalCenter
        }

        RightPanelWhiteButton {
            anchors.verticalCenterOffset: 0
            hasIcon: true
            iconWidth: 14
            iconHeight: 14
            icon: "/images/common/signal.png"
            buttonText: ""
            anchors.verticalCenter: parent.verticalCenter
        }

        Rectangle {
            id: rectangle1
            width: 200
            height: 200
            color: "#00000000"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }

}
