import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

Item {
    width: 225
    height: 138

    property bool isOpen
    isOpen: false

    signal toolBoxOnlineRequested();
    signal toolBoxAwayRequested();
    signal toolBoxBusyRequested();
    signal toolBoxInvisibleRequested();
    signal toolBoxOfflineRequested();
    signal toolBoxRedirectRequested();

    function setDisabledValue(value) {
        toolBoxOnlineMenuItem1.isEnabled=true
        toolBoxOnlineMenuItem2.isEnabled=true
        toolBoxOnlineMenuItem3.isEnabled=true
        toolBoxOnlineMenuItem4.isEnabled=true
        toolBoxOnlineMenuItem5.isEnabled=true
        if(value === "online") {
            toolBoxOnlineMenuItem1.isEnabled=false
        } else if(value === "away") {
            toolBoxOnlineMenuItem2.isEnabled=false
       } else if(value === "busy") {
            toolBoxOnlineMenuItem3.isEnabled=false
       } else if(value === "invisible") {
            toolBoxOnlineMenuItem4.isEnabled=false
       } else if(value === "offline") {
            toolBoxOnlineMenuItem5.isEnabled=false
       }
    }

    property var closeMenu;
    closeMenu: function() {}


    Rectangle {
        visible: isOpen
        focus: isOpen

        id: qqrectangle1
        color: "#DCDCDC"
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        anchors.topMargin: 0
        border.color: "#b4b4b4"
        anchors.fill: parent
        Rectangle {
            id: qqrect5
            color: "#DCDCDC"
            anchors.fill: parent
            ColumnLayout {
                id: columnLayout1
                anchors.fill: parent
                spacing: 0

                ToolBoxOnlineMenuItem {
                    vImage: "online"
                    vName: "Online"
                    id: toolBoxOnlineMenuItem1
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    callbackFunction: function() {closeMenu();toolBoxOnlineRequested();}
                }

                ToolBoxOnlineMenuItem {
                    vImage: "away"
                    vName: "Nepřítomný"
                    id: toolBoxOnlineMenuItem2
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    callbackFunction: function() {closeMenu();toolBoxAwayRequested();}
                }
                ToolBoxOnlineMenuItem {
                    vImage: "busy"
                    vName: "Nerušit"
                    id: toolBoxOnlineMenuItem3
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    callbackFunction: function() {closeMenu();toolBoxBusyRequested()};
                }
                ToolBoxOnlineMenuItem {
                    vImage: "offline"
                    vName: "Neviditelný"
                    id: toolBoxOnlineMenuItem4
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    callbackFunction: function() {closeMenu();toolBoxInvisibleRequested();}
                }
                ToolBoxOnlineMenuItem {
                    vImage: "offline"
                    vName: "Offline"
                    id: toolBoxOnlineMenuItem5
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    callbackFunction: function() {closeMenu();toolBoxOfflineRequested();}
                }

                HLine {
                    height: 5
                    color: "#B4B4B4"
                    Layout.minimumHeight: 5
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                }

                ToolBoxOnlineMenuItem {
                    vImage: "redirect"
                    vName: "Nastavení přesměrování hovorů..."
                    id: toolBoxOnlineMenuItem6
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    callbackFunction: function() {closeMenu();toolBoxRedirectRequested();}
                }
            }
       }
        DropShadow {
            cached:true
            anchors.fill: parent
            horizontalOffset: 2
            verticalOffset: 2
            radius: 8.0
            samples: 17
            color: "#80000000"
            source: qqrect5
        }
    }
}
