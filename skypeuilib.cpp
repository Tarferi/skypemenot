#include "skypeuilib.h"

namespace SkypeUILib {

    QString STATUS_ICONS[]={"online","away","busy","offline","offline","connecting"};

    QString STATUS2STR(STATUS status) {
        return STATUS_ICONS[status];
    }


}
