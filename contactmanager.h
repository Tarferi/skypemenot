#ifndef CONTACTMANAGER_H
#define CONTACTMANAGER_H


#include "skypeuilib.h"

#include "contact.h"

using std::cout;

namespace SkypeUILib {

class ContactManagerData {

public:
    std::unordered_map <std::string,Contact*> contacts;
};

class ContactManager {

public:

    ContactManager(QObject* parent) {
        this->contactPanel=parent->findChild<QObject*>("contactsLeftPanelContacts_contactsRow");
        this->contactHistoryPanel=parent->findChild<QObject*>("ContactsLeftPanelContact_listModel");
    }

    void deleteContact(std::string protocol, std::string username) {
        std::unordered_map<std::string,ContactManagerData*>::iterator it;
        it=data.find(protocol);
        if (it != data.end()) {
            ContactManagerData* qdata=it->second;
            std::unordered_map<std::string,Contact*>::iterator qit;
            qit=qdata->contacts.find(username);
            if(qit != qdata->contacts.end()) {
                qdata->contacts.erase(username);
                Contact* c=qit->second;
                delete c;
            }

        }
    }

    Contact* addContact(std::string protocol, std::string username) {
        std::unordered_map<std::string,ContactManagerData*>::iterator it;
        it=data.find(protocol);
        if (it == data.end()) {
            data[protocol]=new ContactManagerData();
        }
        ContactManagerData* cd=data[protocol];
        std::unordered_map<std::string,Contact*>::iterator qit;
        qit=cd->contacts.find(username);
        if(qit != cd->contacts.end()) {
            Contact* c=qit->second;
            return c;
        } else {
            Contact* c=new Contact(contactPanel, contactHistoryPanel, protocol, username);
            cd->contacts[username]=c;
            return c;
        }
    }

    Contact* getContactByData(std::string protocol, std::string username) {
        std::unordered_map<std::string,ContactManagerData*>::iterator it;
        it=data.find(protocol);
        if (it != data.end()) {
            ContactManagerData* qdata=it->second;
            std::unordered_map<std::string,Contact*>::iterator qit;
            qit=qdata->contacts.find(username);
            if(qit != qdata->contacts.end()) {
                Contact* c=qit->second;
                return c;
            }

        }
        return NULL;
    }

private:
    std::unordered_map <std::string, ContactManagerData*> data;

    QObject* contactPanel;
    QObject* contactHistoryPanel;

    };

}
#endif // CONTACTMANAGER_H
