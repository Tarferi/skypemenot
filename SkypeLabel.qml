import QtQuick 2.0

Text {
    FontLoader { id: skypeFont; source: bold?"fonts/segoe_bold.ttf":"fonts/segoe.ttf" }
    font.bold: bold
    style: Text.Normal
    font.family: skypeFont.name
    font.pixelSize: size
    renderType: Text.NativeRendering

    property int size: 14
    property bool bold: false
}
