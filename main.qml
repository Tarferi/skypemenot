import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick 2.7
import QtQuick.Layouts 1.3

Window {
    id: mainWindow
    visible: true
    width: 590
    height: 530
    title: qsTr("SkypeMeNow")
    color: "white"
    objectName: "MainWindow"

    signal guiLoaded();

    property bool loaded;
    loaded: false;

    property alias toolBox: toolBox1

    SplitView {
        id: mainSplit
        anchors.fill: parent

        Rectangle {
            id: leftPanel
            color: "#00000000"
            Layout.minimumWidth: 190
            Layout.maximumWidth: 500;

            ColumnLayout {
                id: columnLayout1
                height: 120
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 5
                anchors.rightMargin: 5
                anchors.leftMargin: 9
                anchors.topMargin: 4
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: parent.top

                ToolBox {
                    id: toolBox1
                    height: 110
                    Layout.fillHeight: false
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.fillWidth: true
                }

                ContactsLeftPanel {
                    id: contactsLeftPanel1
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    rightPanelParent: rightPanel
                }
            }
        }

        Rectangle {
            id: rightPanel
            anchors.right: parent.right
            anchors.rightMargin: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: mainWindow.width/2
            /*
            RightPanel {
                anchors.fill: parent
            }
            */
        }
    }

}
