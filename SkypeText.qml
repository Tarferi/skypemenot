import QtQuick 2.0

Text {
    property bool bold;
    bold: false;
    property int size: 12;
    FontLoader { id: skypeFont2; source: "fonts/tahoma.ttf" }
    style: Text.Normal
    font.family: skypeFont2.name
    font.pixelSize: size
    font.bold: bold;
    renderType: Text.NativeRendering
}
