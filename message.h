#ifndef MESSAGE_H
#define MESSAGE_H

#include "skypeuilib.h"
#include "messagecontent.h"

namespace SkypeUILib {

    class Message {

    public:
        Message(Contact* creator,unsigned long creationTime) {
            content=new MessageContent();
            this->creator=creator;
            this->creationTime=creationTime;
        }

        void addMessageContent(MessageContentData* data) {
            content->addContentData(data);
        }

        QObject* getObject() {
            return object;
        }


    private:
        MessageContent* content;
        Contact* creator;
        unsigned long creationTime;
        QObject* object;
    };
}
#endif // MESSAGE_H
