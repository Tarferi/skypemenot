#include "messagecontentdata.h"

namespace SkypeUILib {

    QObject* MessageContentData_TEXT::createObject(QString source) {
        QQmlEngine engine;
        QQmlComponent component(&engine, QUrl::fromLocalFile("SkypeText.qml"));
        QObject *object = component.create();
        QQmlProperty::write(object, "text", source);
        return object;
    }

}
