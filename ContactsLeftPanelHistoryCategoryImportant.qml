import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick 2.7
import QtQml 2.2
import QtQml.Models 2.2

Item {

    id: qitem;
    height:qmodel.count*24;

    property alias qcoll: qcol

    function stillBelongs(contact) {
        return contact._hasCallingIcon;
    }

    function resortMe(contact) {
        var children=qmodel.children;
        var item=contact.listItem;
        var length=children.length;
        var found=false;
        var foundIndex=0;
        var foundMe=false;
        var myIndex=0;
        var myTime=contact.lastAction.getTime();
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child === item) {
                foundMe=true;
                myIndex=i;
                if(foundIndex) {
                    break;
                }
            } else if(child.item.lastAction.getTime() < myTime){
                found=true;
                foundIndex=i;
                if(foundMe) {
                    break;
                }
            }
        }
        if(foundMe && foundIndex) {
            qmodel.move(myIndex,foundIndex);
        }
    }

    function moveHere(contact) {
        contact.category=qitem;
        var item=contact.listItem;
        var children=qmodel.children;
        var length=children.length;
        var myTime=contact.lastAction.getTime();
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child.item.lastAction.getTime() < myTime) {
                qmodel.insert(i,item);
                return;
            }
        }
        qmodel.append(item);
    }

    function deleteFrom(item) {
        var children=qmodel.children;
        var length=children.length;
        for(var i=0;i<length;i++) {
            var child=children[i];
            if(child === item) {
                qmodel.remove(i);
                item.destroy();
                return;
            }
        }
        item.destroy();
        console.error("Child not found while removing");
    }


    function update() {
    }


    ObjectModel {
        id: qmodel;

        onCountChanged: {
            qcol.height=count*80;
        }
    }

    ListView {
        model: qmodel
        id: qcol;
        anchors.fill: parent
        orientation: Qt.Vertical
        spacing: 1
    }

}
