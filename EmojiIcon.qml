import QtQuick 2.7

Item {

    id:qitem

    width: 28
    height: 28

    property var emojiName:"smile"
    property var emojiDescription: "blablabla"
    property var emojiShort:""

    signal clicked()
    signal hovered(bool hover)

    Rectangle {
        id:qrect
        width: 26
        height: 26
        border.width: 2
        radius: 2
        color: "transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        border.color: "#ffffff"

        AnimatedImage {
            playing: false
            mipmap: true
            smooth: true
            id:qimg
            source: "/images/emojis/"+emojiName+".gif"
            width: 20
            height: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Component.onCompleted: {
                qimg.currentFrame=8
            }
        }

    }

    property bool hover: false

    MouseArea {
        anchors.fill: qrect
        hoverEnabled: true
        onClicked: {
            qitem.clicked();
        }
        onEntered: {
            hover=true
            qimg.playing=true
            hovered(true);
        }
        onExited: {
            hover=false
            qimg.playing=false
            qimg.currentFrame=8
            hovered(false);
        }
    }

    states: [
        State {
            name:"DEFAULT"
            when: !hover
            PropertyChanges {target: qrect; border.width: 0}
        },
        State {
            name:"HOVER"
            when: hover
            PropertyChanges {target: qrect; border.width: 2}
        }

    ]

}
