import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

SkypeMenu {

    signal toolBoxOnlineRequested();
    signal toolBoxAwayRequested();
    signal toolBoxBusyRequested();
    signal toolBoxInvisibleRequested();
    signal toolBoxOfflineRequested();
    signal toolBoxRedirectRequested();

    realWidth: 225;
    //implicitHeight: 132

    x:0
    y:25

    function setDisabledValue(value) {
        m1.enabled=true
        m2.enabled=true
        m3.enabled=true
        m4.enabled=true
        m5.enabled=true
        if(value === "online") {
            m1.enabled=false
        } else if(value === "away") {
            m2.enabled=false
       } else if(value === "busy") {
            m3.enabled=false
       } else if(value === "invisible") {
            m4.enabled=false
       } else if(value === "offline") {
            m5.enabled=false
       }
    }

    ToolBoxOnlineMenuItem {
        id: m1
        text: "Online"
        icon: "/images/status/online"
        onTriggered: toolBoxOnlineRequested;
    }

    ToolBoxOnlineMenuItem {
        id: m2
        text: "Nepřítomný"
        icon: "/images/status/away"
        onTriggered: toolBoxAwayRequested;
    }

    ToolBoxOnlineMenuItem {
        id: m3
        text: "Nerušit"
        icon: "/images/status/busy"
        onTriggered: toolBoxBusyRequested;
    }

    ToolBoxOnlineMenuItem {
        id: m4
        text: "Neviditelný"
        icon: "/images/status/offline"
        onTriggered: toolBoxInvisibleRequested;
    }

    ToolBoxOnlineMenuItem {
        id: m5
        text: "Offline"
        icon: "/images/status/offline"
        onTriggered: toolBoxOfflineRequested;
    }

    SkypeMenuSeparator {

    }

    ToolBoxOnlineMenuItem {
        id: m6
        text: "Nastavení přesměrování hovorů..."
        icon: "/images/status/redirect"
        onTriggered: toolBoxRedirectRequested;
    }


    /*
    function popup() {
        visible=true
        focus=true
    }
    function popdown(){
        visible=false
    }

    function setDisabledValue(value) {
        toolBoxStatusMenu.setDisabledValue(value);
    }

    property alias innerMenu: toolBoxStatusMenu

    visible:false
    implicitWidth: 225
    implicitHeight: 138
    Rectangle {
        x:0
        y:0
        width:parent.width
        height:parent.height
        color: "#00000000"
        ToolBoxOnlineMenuWrapper {
            id: toolBoxStatusMenu
            objectName: "obj_toolbox_menu"
            x:0
            y:0
            width:225
            height:138
            isOpen: true
            closeMenu: function() {
                popdown();
            }
        }
    }*/

}
