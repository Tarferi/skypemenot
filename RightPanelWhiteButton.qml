import QtQuick 2.0

SkypeButton {
    defaultColor: "#ffffff"
    hoverColor: "#E5E6E6"
    clickedColor: "#CECED0"
    borderWidth: 1
    borderColor: "#ADAEB0"
}
