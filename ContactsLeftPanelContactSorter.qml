import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    width: 220
    height: 30

    signal filterRequest(string protocol);

    property var lastSelected: fall

    Component.onCompleted: {
        lastSelected.checked=true
        text1.text=lastSelected.text
    }

    function filter(item, protocol) {
        if(lastSelected !== item) {
            filterRequest(protocol);
            lastSelected.checked=false
            lastSelected=item
            lastSelected.checked=true
            text1.text=item.text
        }
    }

    SkypeMenu {
        id: filterMenu
        width: 89

        x:52
        y:20

        ContactsLeftPanelFilterMenuItem {
            id:fall;
            text: "Všechny"
            onTriggered: {filter(fall, "all");}
        }
        ContactsLeftPanelFilterMenuItem {
            id:fskype;
            text: "Skype"
            onTriggered: {filter(fskype, "skype");}
        }
        ContactsLeftPanelFilterMenuItem {
            id:fonline;
            text: "Online"
            onTriggered: {filter(fonline, "online");}
        }
        ContactsLeftPanelFilterMenuItem {
            id:fda;
            text: "Digital Addiction"
            onTriggered: {filter(fda, "da");}
        }
        ContactsLeftPanelFilterMenuItem {
            id:fvenom;
            text: "Venom7"
            onTriggered: {filter(fvenom, "venom7");}
        }
        ContactsLeftPanelFilterMenuItem {
            id: fcassie
            text: "Cassie"
            onTriggered: {filter(fcassie, "cassie");}
        }
        ContactsLeftPanelFilterMenuItem {
            id:ffb;
            text: "Facebook"
            onTriggered: {filter(ffb, "facebook");}
        }
        ContactsLeftPanelFilterMenuItem {
            id:ficq;
            text: "ICQ"
            onTriggered: {filter(ficq, "icq");}
        }
    }

    Rectangle {
        id: rectangle299
        color: "#00d5d7d9"
        radius: 4
        anchors.rightMargin: 4
        anchors.leftMargin: 4
        anchors.bottomMargin: 2
        anchors.topMargin: 4
        anchors.fill: parent


        ParallelAnimation {
            id: animateColorIn9
            PropertyAnimation {target: rectangle299; properties: "color"; to: "#ffd5d7d9"; duration: 250}
            PropertyAnimation {target: skypeText1; properties: "opacity"; to: 1; duration: 100}
        }

        ParallelAnimation {
            id: animateColorOut9
            PropertyAnimation {target: rectangle299; properties: "color"; to: "#00d5d7d9"; duration: 250}
            PropertyAnimation {target: skypeText1; properties: "opacity"; to: 0; duration: 100}
        }

        RowLayout {
            id: rowLayout1
            spacing: 5
            anchors.leftMargin: 3
            anchors.fill: parent
            clip: true

            SkypeLabel {
                id: text1
                text: "Online"
                Layout.fillHeight: false
            }

            Image {
                id: image1
                Layout.maximumWidth: 7
                Layout.maximumHeight: 4
                Layout.minimumWidth: 7
                Layout.minimumHeight: 4
                source: "images/common/arrowdown.png"
            }

            Rectangle {
                id: rectangle1
                color: "#00000000"
                Layout.maximumHeight: 20
                Layout.fillWidth: true
            }

            SkypeText {
                id: skypeText1
                color: "#999999"
                text: "Změnit seznam kontaktů"
                opacity:0
            }
        }
        MouseArea {
            anchors.fill: rectangle299
            hoverEnabled: true
            onEntered: {animateColorIn9.start();animateColorOut9.stop();}
            onExited: {animateColorIn9.stop();animateColorOut9.start();}
            onClicked: {
                filterMenu.popup();
            }
        }

    }


}
