import QtQuick 2.0
import QtQuick.Controls 2.0

MenuItem {

    id:qitem

    property bool hasOwnIcon: false
    property string icon:"/images/status/busy";
    property string _icon: icon+".png"
    //property string _iconSelected: icon+"_sel.png"
    property string _iconSelected: _icon;

    property int iconWidth: 16
    property int iconHeight: 16

    property int iconOffset: 5
    property int textOffset: 5
    property int checkOffset: 5

    property int itemFontSize: 12;
    property int itemHeight: 25

    property color defaultTextColor: "#000000"
    property color hoverTextColor: "#ffffff"
    property color clickedTextColor: hoverTextColor

    property color defaultBackground: "transparent"
    property color hoverBackground: "#5A99D7"
    property color clickedBackground: hoverBackground

    property int defaultBorderWidth: 2
    property int hoverBorderWidth: 2
    property int clickedBorderWidth: hoverBorderWidth

    property color defaultBorderColor: "transparent"
    property color hoverBorderColor: "#22628F"
    property color clickedBorderColor: hoverBorderColor

    property real disabledOpacity: 0.3

    property bool isSubmenu: false;
    property var submenuObject: null;
    property var _parentMenuItem: null;

    width: parent.width;
    height: itemHeight;

    checkable: false
    checked: false

    property int checkWidth: 8
    property int checkHeight: 9


    indicator: Rectangle {
        width:0
        height:0
    }

    Timer {
        id:timer
        property bool changed: false
        property bool counting: false
        interval: 700
        repeat: false
        onTriggered: {
            if(!changed) {
                if(submenuObject !== null) {
                    submenuObject.x=width;
                    submenuObject.y=-2
                    submenuObject.open()
                }
            }
            counting=false
        }
        function begin() {
            if(counting) {
                stop();
            }
            counting=true
            changed=false
            start();
        }
    }

    Timer {
        id:dselTimer
        interval: 300
        property bool counting: false
        repeat: false
        onTriggered: {
            counting=false
            //console.log("Submenu has mouse: "+submenuObject.hasMouse);
            if(!submenuObject.hasMouse) {
                submenuObject.close();
                qib.hasMouse=false
            }
        }
        function begin() {
            if(counting) {
                stop();
            }
            counting=true
            start();
        }
    }

    states: [
        State {
            name:"DEFAULT"
            when: !qib.hasMouse && qitem.enabled
            PropertyChanges {target: qib;color:defaultBackground}
            PropertyChanges {target: idll;color:defaultTextColor}
            PropertyChanges {target: qib;border.width: defaultBorderWidth}
            PropertyChanges {target: qib;border.color: defaultBorderColor}
            PropertyChanges {target: qicon;source: _icon}
            PropertyChanges {target: cicon;source: "/images/common/check.png"}
        }, State {
            name:"SELECTED"
            when: qib.hasMouse && qitem.enabled
            PropertyChanges {target: qib;color:hoverBackground}
            PropertyChanges {target: idll;color:hoverTextColor}
            PropertyChanges {target: qib;border.width: hoverBorderWidth}
            PropertyChanges {target: qib;border.color: hoverBorderColor}
            PropertyChanges {target: qicon;source: _iconSelected}
            PropertyChanges {target: cicon;source: "/images/common/check_sel.png"}
        }, State {
            name:"CLICKED"
            when: qitem.down && qitem.enabled
            PropertyChanges {target: qib;color:clickBackground}
            PropertyChanges {target: idll;color:clickedTextColor}
            PropertyChanges {target: qib;border.width: clickedBorderWidth}
            PropertyChanges {target: qib;border.color: clickedBorderColor}
            PropertyChanges {target: qicon;source: _iconSelected}
            PropertyChanges {target: cicon;source: "/images/common/check_sel.png"}
        }, State {
            name:"DISABLED"
            when: !qitem.enabled
            PropertyChanges {target: qib;color:defaultBackground}
            PropertyChanges {target: idll;color:defaultTextColor}
            PropertyChanges {target: qib;border.width: defaultBorderWidth}
            PropertyChanges {target: qib;border.color: defaultBorderColor}
            PropertyChanges {target: qicon;source: _icon}
            PropertyChanges {target: cicon;source: "/images/common/check.png"}

            PropertyChanges {target: qicon;opacity: disabledOpacity}
            PropertyChanges {target: cicon;opacity: disabledOpacity}
            PropertyChanges {target: idll;opacity: disabledOpacity}

        }
    ]

    contentItem: Rectangle {
        anchors.fill: parent
        id: idl
        SkypeText {
            id:idll
            text: qitem.text
            anchors.verticalCenter: parent.verticalCenter
            x: {
                var def=textOffset
                if(hasOwnIcon){
                    def+=iconOffset+iconWidth
                }
                if(qitem.checkable){
                    def+=checkOffset+checkWidth
                }
                return def
            }
            width: parent.width;
            size: itemFontSize
        }

        Image {
            id:qicon
            width: iconWidth
            height: iconHeight
            x: {
               var def=iconOffset
               if(qitem.checkable){
                   def+=checkOffset+checkWidth
               }
               return def
           }
            anchors.verticalCenter: parent.verticalCenter
            source: _icon
            visible: hasOwnIcon
        }
        Image {
            id:cicon
            width: checkWidth
            height: checkHeight
            x: checkOffset
            anchors.verticalCenter: parent.verticalCenter
            visible: qitem.checkable && qitem.checked
        }
        Image {
            id:sicon
            width: 4
            height: 7
            anchors.rightMargin: 5
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            source: "/images/common/submenuArrow.png"
            visible: isSubmenu
        }

        color: "transparent"
    }

    background: Rectangle {
        anchors.fill: parent
        id:qib
        property bool hasMouse:false
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                if(_parentMenuItem !== null) {
                    _parentMenuItem.hasMouse=true
                }
                qib.hasMouse=true;
                timer.changed=timer.counting
                timer.begin()
                qitem.parent.hasMouse=true
            }
            onExited: {
                if(isSubmenu) {
                    dselTimer.begin();
                } else {
                    qib.hasMouse=false
                    timer.changed=true
                }
            }
            onClicked: {
                if(!isSubmenu) {
                    qitem.triggered();
                }
            }
        }
    }

}
