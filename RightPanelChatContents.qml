import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQml.Models 2.2

Item {

    id: qitem

    Component.onCompleted: {
        console.log("QW: "+scr.left)
    }

    function appendMessage(message) {
        qmodel.append(message);
    }

    function appendDate(dater) {
        qmodel.append(dater)
        dater.width=Qt.binding(function() {return qitem.width})
    }

    function scrollToBottom() {
        scr.flickableItem.contentY = scr.flickableItem.contentHeight
    }

    ScrollView {
        id:scr
        anchors.fill: parent

        ListView {
            interactive: false
            anchors.fill: parent
            id:lst
            spacing: 10

            model: ObjectModel {
                id:qmodel

                onChildrenChanged: {
                    var children=qmodel.children
                    if(children.length > 0) {
                        var child=children[children.length-1];
                        child.scroller=qitem
                        scrollToBottom();
                    }
                }
            }
        }
    }



}
