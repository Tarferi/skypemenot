import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {

    id:qitem;

    property var conv;

    property var lastContent:null;
    property var myUsername:""

    anchors.fill: parent

    //signal messageSent(string message);

    function messageSent(message) {
        appendMessage("skype","Drew Parker","tav.taferi",new Date(),message);
    }

    property var lastDateText:null

    property alias conversationIcon: rightPanelTop1.conversationIcon
    property alias conversationName: rightPanelTop1.conversationName

    function appendMessage(protocol, authorDisplayName, authorUsername, date, content) {
        var edit=false
        if(lastDateText === null){
            edit=true
        } else if(!lastDateText.stillActual()) {
            edit=true
        }
        if(edit) {
            var bobj=Qt.createComponent("RightPanelMessageDate.qml")
            lastDateText=bobj.createObject();
            lastDateText.date=date
            rightPanelChatPanel1.appendDate(lastDateText);
        }
        var isMe= myUsername === authorUsername
        if(lastContent === null) {
            var dobj=Qt.createComponent("RightPanelMessage.qml")
            lastContent=dobj.createObject();
            lastContent.authorUsername=authorUsername
            lastContent.authorDisplayName=authorDisplayName
            lastContent.isMe=isMe
            rightPanelChatPanel1.appendMessage(lastContent);
        } else if (lastContent.authorUsername !== authorUsername) {
            var eobj=Qt.createComponent("RightPanelMessage.qml")
            lastContent=eobj.createObject();
            lastContent.authorUsername=authorUsername
            lastContent.authorDisplayName=authorDisplayName
            lastContent.isMe=isMe
            rightPanelChatPanel1.appendMessage(lastContent);
        }
        var cobj=Qt.createComponent("RightPanelMessageContents.qml")
        var message=cobj.createObject();
        message.date=date
        message.isNew=!isMe
        message.text=content
        lastContent.addMessage(message);
        lastContent=null
    }



    ColumnLayout {
        id: columnLayout1
        anchors.topMargin: 8
        spacing: 0
        anchors.fill: parent

        RightPanelTop {
            id: rightPanelTop1
            Layout.fillWidth: true
            Layout.rightMargin: 8
            Layout.leftMargin: 8
        }

        HLine2 {
            realHeight: 1
            Layout.fillWidth: true
            Layout.rightMargin: 8
            Layout.leftMargin: 8
        }

        RightPanelButtons {
            id: rightPanelButtons1
            Layout.fillWidth: true
            Layout.rightMargin: 8
            Layout.leftMargin: 8
        }

        HLine2 {
            realHeight: 1
            Layout.fillWidth: true
        }

        RightPanelChatPanel {
            onMessageSent: function(msg) {qitem.messageSent(msg);}
            id: rightPanelChatPanel1
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

    }

}
