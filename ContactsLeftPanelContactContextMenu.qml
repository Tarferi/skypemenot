import QtQuick 2.0

SkypeMenu {
    id:myContext
    property var contact;

    function repos(x,y) {
        myContext.x=x
        myContext.y=-550
    }

    property int theY:0

    Component.onCompleted: {
        theY=implicitHeight*2
    }

    realWidth: 185

    ContactsLeftPanelContactContextMenuItem {
        text: "Hovor"
        onTriggered: contact.callClicked();
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Zahájit chat"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Odeslat video zprávu"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Poslat SMS zprávu"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Odeslat kontakty..."
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Odeslat soubory..."
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Odeslat hlasovou zprávu"
        enabled: false
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Sdílení obrazovek"
    }
    SkypeMenuSeparator {}
    ContactsLeftPanelContactContextMenuItem {
        text: "Zobrazit profil"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Přejmenovat..."
    }
    SkypeMenuSeparator {}
    ContactsLeftPanelContactContextMenuItem {
        text: "Přidat do oblíbených"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Přidat do kategorie"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Blokovat tuto osobu..."
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Zobrazit staré zprávy"
        isSubmenu: true
        submenuObject: SkypeMenu {
            ContactsLeftPanelContactContextMenuItem {
                text: "1 den"
            }
        }
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Odebrat z kontaktů"
    }
    SkypeMenuSeparator {}
    ContactsLeftPanelContactContextMenuItem {
        text: "Skrýt konverzaci"
    }
    SkypeMenuSeparator {}
    ContactsLeftPanelContactContextMenuItem {
        text: "Upravit této osobě profil"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Zablokovat tento účet"
    }
    ContactsLeftPanelContactContextMenuItem {
        text: "Odhlásit tuto osobu"
    }
}

