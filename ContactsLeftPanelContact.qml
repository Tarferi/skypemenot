import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    width: 220
    height: 40

    property var contacter;
    property var listItem: null;

    property string username
    property string description
    property string statusIcon

    property string protocol;
    property string displayName;

    property string displayedName;

    property bool isCalling: false;

    property bool isCallingIconVisible: false

    function update() {
        if(displayName == "") {
            displayedName=username;
        } else {
            displayedName=displayName;
        }
        usernametext.text=displayedName;
    }

    property bool hasUnreadMessages:false

    Component.onCompleted: {
        update();
        _setCall(isCalling, true);
    }

    onIsCallingIconVisibleChanged: {
        contacter.setCalling(contactroot,isCallingIconVisible);
    }

    onUsernameChanged: {
        update();
    }

    onDisplayNameChanged: {
        update();
    }

    displayedName: "";
    statusIcon: "connecting"

    signal callEndClicked()

    signal qcte()

    signal callClicked();

    onIsCallingChanged: {
        setCall(isCalling);
        if(isCalling) {
            isCallingIconVisible=true;
        }
    }

    function setIsCalling(call) {
        isCalling=call;
    }

    function setCall(call) {
        _setCall(call,false);
    }

    function _setCall(call, forced) {
        if(call) {
            animateColorOut7.stop();
            qstatusIconCall.reset()
            callRect.opacity=1;
            qstatusIcon.visible=false
            qstatusIconCall.visible=true
            callRect.visible=true
        } else {
            callRect.visible=false
            if(!forced) {
                animateColorOut7.start();
                qstatusIconCall.animate()
            } else {
                qstatusIconCall.visible=false
                qstatusIcon.visible=true
                isCallingIconVisible=false
            }
            callRect.opacity=1;
        }
    }

    property var clickedCB;
    clickedCB: function(source){}

    function setSelected(selected){
        if(selected){
            rectangle1.state="SELECTED"
        } else {
            rectangle1.state="NORMAL"
        }
    }

    id: contactroot;

    Rectangle {
        id: rectangle1
        color: "#00000000"
        anchors.fill: parent

        state: "NORMAL"
        states: [
            State {
                name: "NORMAL"
                PropertyChanges {target: rectangle1; color: "transparent"}
                PropertyChanges {target: usernametext; color: "#000000"}
                PropertyChanges {target: descrtext; color: "#999999"}
                StateChangeScript {
                    name:"setSel1"
                    script: qstatusIconCall.setSelected(false);
                }

            },
            State {
                name: "SELECTED"
                PropertyChanges {target: rectangle1; color: "#0095CC"}
                PropertyChanges {target: usernametext; color: "#ffffff"}
                PropertyChanges {target: descrtext; color: "#ffffff"}
                StateChangeScript {
                    name:"setSel1"
                    script: qstatusIconCall.setSelected(true);
                }
            }
        ]
        ContactsLeftPanelContactContextMenu {
            id:myContext
            contact:contactroot
        }


        MouseArea {
            anchors.fill: parent;
            onClicked: {
                if(mouse.button & Qt.RightButton) {
                    clickedCB(contactroot);
                    myContext.repos(mouse.x,mouse.y);
                    myContext.popup();
                } else {
                    clickedCB(contactroot);
                }
            }
            acceptedButtons: Qt.LeftButton | Qt.RightButton
        }

        RowLayout {
            id: rowLayout1
            anchors.right: parent.right
            anchors.rightMargin: 4
            spacing: 9
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.verticalCenter: parent.verticalCenter

            Image {
                id: avatar
                Layout.maximumWidth: 32
                Layout.maximumHeight: 32
                source: "images/common/defaultUser.png"
            }

            Rectangle {
                id: rectangle2
                color: "#00000000"
                Layout.fillHeight: true
                Layout.fillWidth: true

                RowLayout {
                    id: rowLayout2
                    spacing: 9
                    anchors.topMargin: 1
                    anchors.fill: parent
                    Rectangle {
                        color: "#00000000"
                        Layout.maximumHeight: 16
                        Layout.maximumWidth: 16
                        Layout.minimumHeight: 16
                        Layout.minimumWidth: 16
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Image {
                            id: qstatusIcon
                            Layout.maximumHeight: 16
                            Layout.maximumWidth: 16
                            source: "images/status/"+statusIcon+".png"
                            visible:false
                        }
                        AnimatedCallIcon {
                            id: qstatusIconCall
                            Layout.maximumHeight: 16
                            Layout.maximumWidth: 16
                            visible:true
                            finishCallback: function() {
                                qstatusIconCall.visible=false
                                qstatusIcon.visible=true
                                isCallingIconVisible=false;
                            }
                        }
                    }
                    ColumnLayout {
                        id: columnLayout1
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        SkypeText {
                            id: usernametext
                            text: displayedName
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        }

                        SkypeText {
                            id: descrtext
                            color: "#999999"
                            text: qsTr(description)
                        }
                    }

                    Rectangle {
                        id: rectunreadwrap
                        width:16
                        height:16
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        color: "transparent"
                        Rectangle {
                            width:8
                            height:8
                            radius:4
                            id: rectunread
                            color: "#FF8C00"
                            visible: hasUnreadMessages
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }

                    Rectangle {
                        id: callRect
                        Layout.maximumHeight: 16
                        Layout.maximumWidth: 16
                        color: "#00000000"
                        Layout.minimumHeight: 16
                        Layout.minimumWidth: 16
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop



                        Image {
                            id: image1
                            width: 16
                            height: 16
                            anchors.fill: parent
                            source: "images/status/call2.png"
                            opacity:1
                        }

                        Image {
                            id: image2
                            width: 16
                            height: 16
                            anchors.fill: parent
                            source: "images/status/call2_hover.png"
                            opacity:0
                        }

                        Image {
                            id: image3
                            width: 16
                            height: 16
                            anchors.fill: parent
                            source: "images/status/call2_click.png"
                            opacity:0
                        }

                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                rectangle1.forceActiveFocus()
                                callEndClicked();
                            }
                            onEntered: {animateColorOut5.stop();animateColorIn5.start();}
                            onExited: {animateColorIn5.stop();animateColorOut5.start();}
                            onPressed: {animateColorIn6.start();}
                        }
                    }


                    PropertyAnimation {id: animateColorIn5; target: image2; properties: "opacity"; to: 1; duration: 250}
                    PropertyAnimation {id: animateColorOut5; target: image2; properties: "opacity"; to: 0; duration: 250}

                    PropertyAnimation {id: animateColorIn6; target: image3; properties: "opacity"; to: 1; duration: 25}
                    PropertyAnimation {id: animateColorOut7; target: callRect; properties: "opacity"; to: 0; duration: 300;}

                }
            }
        }
    }

}
