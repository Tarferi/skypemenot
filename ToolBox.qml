import QtQuick 2.7
import QtQuick.Layouts 1.3

Item {
    id: item1
    width: 160
    height: 160

    objectName: "obj_toolbox"

    function deselectAllButtons() {
        toolBoxButtons1.toolBoxSetHomeSelected(false);
        toolBoxButtons1.toolBoxSetCallSelected(false);
        toolBoxButtons1.toolBoxSetFriendsSelected(false);
        toolBoxButtons1.toolBoxSetAddSelected(false);
        toolBoxMe1.setSelftSelected(false);
    }

    signal toolBoxOnlineRequested();
    signal toolBoxAwayRequested();
    signal toolBoxBusyRequested();
    signal toolBoxInvisibleRequested();
    signal toolBoxOfflineRequested();
    signal toolBoxRedirectRequested();

    ColumnLayout {
        id: columnLayout1
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        spacing: 0
        anchors.fill: parent

        ToolBoxMe {
            id: toolBoxMe1
            z: 1
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        }

        HLine {
            height: 2
            color: "#d9d9d9"
            Layout.fillWidth: true
        }

        ToolBoxButtons {
            id: toolBoxButtons1
            Layout.fillWidth: true
        }

        ToolBoxSearch {
            id: toolBoxSearch1
            Layout.fillWidth: true
        }

    }
}

