import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {

    id:qitem
    width:205
    height: childrenRect.height

    property var callbackInserter:null

    property var mappings:[]
    property var assocs: []

    Component.onCompleted: {
       _generateAll()
       _addAll()
       _generateAssoc();
    }

    function getShortByFile(file) {
        if(mappings[file]) {
            return mappings[file][0][0];
        } else {
            console.error("Seeking non existing file icon: "+file)
            return "[25: IT WASN'T FOUND LOL]"
        }
    }

    function _generateAssoc() {
        for (var file in mappings) {
            var arr=mappings[file];
            var sh=arr[0]
            for(var sf in sh) {
                var sform=sh[sf]
                assocs[sform]=file
                //console.log(sform+" => "+file);
            }
        }
    }

    function _addAll() {
        for (var file in mappings) {
            var arr=mappings[file];
            var sh=arr[0][0]
            var desc=arr[1]
            model.append({"name": file,"shortform": sh, "desc": desc});
        }
    }

    function _generateAll() {
        mappings["smile"]=[[":)",":=)",":-)"], "Úsměv"]

        mappings["sad"]=[[":(",":=(",":-("], "Smutek"]
        mappings["laugh"]=[[":D",":=D",":-D",":d",":=d",":-d"], "Velký úsměv"]
        mappings["cool"]=[["(cool)","8=)","8-)","B=)","B-)"], "Pohoda"]
        mappings["surprised"]=[[":O",":=O",":-O",":o",":=o",":-o"], "Překvapení"]
        mappings["wink"]=[[";)",";-)",";=)"], "Mrkám"]
        mappings["cry"]=[[";(",";-(",";=("], "Pláču"]
        mappings["sweat"]=[["(:|","(sweat)"], "Potím se"]

        mappings["silence"]=[[":|",":-|",":=|"], "Mlčím"]
        mappings["kiss"]=[["(kiss)",":*",":=*",":-*"], "Líbám"]
        mappings["tongue"]=[[":P",":=P",":-P",":p",":=p",":-p"], "Vypláznutý jazyk"]
        mappings["blush"]=[[":$","(blush)",":-$",":=$",":''>",":\">"], "Stydím se"]
        mappings["curious"]=[[":^)"], "Zvědavý(á)"]
        mappings["sleepy"]=[["|-)","(snooze)","l-)","l=)"], "Ospalý(á)"]
        mappings["dull"]=[["|-(","|(","|=("], "Nechápu"]
        mappings["inlove"]=[["(inlove)"], "Zamilovaný(á)"]

        mappings["grin"]=[["]:)",">:)","(grin)"], "Šklebím se"]
        mappings["fingers"]=[["(yn)"], "Držím palce"]
        mappings["yawn"]=[["(yawn)"], "Zívám"]
        mappings["puke"]=[["(puke)",":&",":-&",":=&"], "Zvracím"]
        mappings["doh"]=[["(doh)"], "Sakra!"]
        mappings["angry"]=[["(angry)",":@",":-@",":=@","x(","x-(","x=(","X(","X-(","X=("], "Rozzlobený"]
        mappings["notme"]=[["wasntme"], "To já ne!"]
        mappings["party"]=[["(party)"], "Party"]

        mappings["worried"]=[["(worried)",":S", ":-S", ":=S", ":s", ":-s", ":=s"], "Ztrápený(á)"]
        mappings["mmm"]=[["(mm)"], "Mmmmm..."]
        mappings["nerd"]=[["(nerd)", "8-|", "B-|", "8|", "B|", "8=|", "B=|"], "Nerd"]
        mappings["lips"]=[[":x", ":-x", ":X", ":-X", ":#", ":-#", ":=x", ":=X", ":=#"], "Mlčím"]
        mappings["hi"]=[["(wave)","(hi)"], "Hoy"]
        mappings["facepalm"]=[["(facepalm)"], "To snad není možné"]
        mappings["devil"]=[["(devil)"], "Ďábel"]
        mappings["angel"]=[["(angel)"], "Anděl"]

        mappings["envy"]=[["(envy)"], "Závidím"]
        mappings["wait"]=[["(wait)"], "Čekej"]
        mappings["hug"]=[["(hug)"], "Objímám"]
        mappings["makeup"]=[["(makeup)","kate"], "Líčím se"]
        mappings["giggle"]=[["(chuckle)","(giggle)"], "Hihňám se"]
        mappings["clapping"]=[["(clap)"], "Tleskám"]
        mappings["thinking"]=[["(think)", ":?", ":-?", ":=?"], "Přemýšlím"]
        mappings["bowing"]=[["(bow)"], "Klaním se"]

        mappings["rofl"]=[["(rofl)"], "Válím se smíchy po podlaze"]
        mappings["relieved"]=[["(whew)"], "Oddechnul(a)"]
        mappings["happy"]=[["(happy)"], "Šťastný(á)"]
        mappings["smirking"]=[["(smirk)"], "Culím se"]
        mappings["nodding"]=[["(nod)"], "Souhlas"]
        mappings["shaking"]=[["(shake)"], "Nesouhlas"]
        mappings["waiting"]=[["(waiting)"], "Čekám"]
        mappings["emo"]=[["(emo)"], "Emo"]

        mappings["yes"]=[["(y)", "(Y)", "(ok)"], "Ano"]
        mappings["no"]=[["(n)", "(N)"], "Ne"]
        mappings["shake"]=[["(handshake)"], "Domluveno"]
        mappings["high5"]=[["(highfive)"], "Plácnem si"]
        mappings["heart"]=[["(heart)", "(h)", "(H)", "(l)", "(L)"], "Srdce"]
        mappings["lalala"]=[["(lalala)"], "Neslyším"]
        mappings["heidy"]=[["(heidy)"], "Heidy"]
        mappings["flower"]=[["(F)","(f)"], "Květina"]

        mappings["rain"]=[["(rain)","(london)","(st)"], "Prší"]
        mappings["sun"]=[["(sun)"], "Slunce"]
        mappings["tumbleweed"]=[["(tumbleweed)"], "Ticho před bouří"]
        mappings["music"]=[["(music)"], "Hudba"]
        mappings["bandit"]=[["(bandit)"], "Bandita"]
        mappings["tmi"]=[["(TMI)"], "Příliž mnoho informací"]
        mappings["cafee"]=[["(coffee)"], "Kafe"]
        mappings["pizza"]=[["(pizza)","(pi)"], "Pizza"]

        mappings["cash"]=[["(cash)","(mo)","($)"], "Prachy"]
        mappings["muscle"]=[["(flex)", "(muscle)"], "Svaly"]
        mappings["cake"]=[["(^)","(cake)"], "Dort"]
        mappings["beer"]=[["(beer)"], "Pivo"]
        mappings["drink"]=[["(d)","(D)"], "Drink"]
        mappings["dancing"]=[[ "\o/",	"(dance)", "\:D/", "\:d/"], "Tančím"]
        mappings["ninja"]=[["(ninja)"], "Nindža"]
        mappings["star"]=[["(*)"], "Hvězda"]

    }

    ListModel {
        id: model
    }


    Rectangle {
        id:qrect5
        width: 205
        height: 265
        radius: 1
        color: "#bf000000"
        border.width: 1
        border.color: "#cc000000"

        Grid {
            id: grid1
            spacing: -3
            width: parent.width
            height: 239
            rows: 9
            columns: 8
            Repeater {
                model: model
                delegate: EmojiIcon {
                    emojiName: name;
                    emojiDescription: desc;
                    emojiShort: shortform;

                    onHovered: function(hover) {
                        if(hover) {
                            t_name.text=desc
                            t_desc.text=emojiShort
                        } else {
                            t_name.text=""
                            t_desc.text=""
                        }
                    }
                    onClicked: function() {
                        if(callbackInserter !== null) {
                            callbackInserter(name, shortform);
                        }
                    }
                }
            }
        }

        HLine2 {
            id: hline
            y:grid1.y+grid1.height
            x:4
            width: parent.width-8
            realBackground: "#ffffff"
        }

        Rectangle {
            y:hline.y+hline.height
            id: rectangle1
            height: 16
            color: "transparent"
            x:4
            width: parent.width-8

            SkypeText {
                id: t_name
                color: "#ffffff"
                text: ""
                anchors.left: parent.left
                size: 11
            }
            SkypeText {
                id: t_desc
                color: "#ffffff"
                text: ""
                anchors.right: parent.right
                size: 11
            }
        }
    }

}
