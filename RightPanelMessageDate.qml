import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {

    property date date: new Date()

    property int margin: 5

    height: childrenRect.height+margin

    readonly property int day: 24*60*60*1000

    property string str1:""
    property string str2:""

    property var scroller:null

    function stillActual() {
        var now=new Date();
        return (date.getTime()+day > now.getTime())
    }

    function _updateDate() {
        var now=new Date().getTime();
        var then=date.getTime();
        if(then + day > now) {
            str1="Dnes";
            str2=date.toLocaleDateString(Qt.locale(),"dddd d. MMMM yyyy");
        } else if(then + day +day > now) {
            str1="Včera";
            str2=date.toLocaleDateString(Qt.locale(),"dddd d. MMMM yyyy");
        } else {
            str1=date.toLocaleDateString(Qt.locale(),"dddd d. MMMM yyyy");
            str2=""
        }
    }

    Component.onCompleted: {
        _updateDate();
    }

    onDateChanged: {
        _updateDate();
    }

    ColumnLayout {
        id: columnLayout1
        anchors.left: parent.left
        anchors.right: parent.right

        Rectangle {
            id: hline1
            Layout.fillWidth: true
            height: 5

            Rectangle {
                id: rectangle1
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                height: 1
                color: "#E5E9EE"
            }
        }

        RowLayout {
            id: rowLayout1
            Layout.fillWidth: true
            height: 50

            Image {
                id: image1
                sourceSize.height: 11
                sourceSize.width: 11
                source: "images/common/clock.png"
            }

            SkypeTextE {
                id: skypeLabel1
                text: str1
                bold: true
                size: 11
            }

            SkypeTextE {
                id: skypeLabel2
                text: str2
                Layout.fillHeight: false
                bold: false
                size: 11
            }
        }

        Rectangle {
            id: hline2
            Layout.fillWidth: true
            height: 5
            color: "transparent"

            Rectangle {
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                height: 1
                color: "#E5E9EE"
            }
        }
    }

}
